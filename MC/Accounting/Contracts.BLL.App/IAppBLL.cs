﻿using System;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        //IContactService Contacts { get; }
        //IContactTypeService ContactTypes { get; }
        IHoldingCompanyService HoldingCompanies { get; }
        IBankService Banks { get; }
        IStatementSheetService Statements { get; }
        IFiscalYearService FiscalYears { get; }
        ICommodityService Commodities { get; }
        // TODO: Public facing services
        // IContactBookService ContactBook
    }
}