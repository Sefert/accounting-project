using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface IStatementSheetService : IBaseEntityService<BLLAppDTO.BLLStatementSheetDTO>, IStatementSheetRepository<BLLAppDTO.BLLStatementSheetDTO>
    {
        Task<IEnumerable<BLLAppDTO.BLLStatementSheetDTO>> GetAllStatementsAsync();
        Task<IEnumerable<BLLAppDTO.BLLStatementSheetDTO>> GetStatementReportByHoldingCompanyId(int id);
    }
}