using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface IHoldingCompanyService : IBaseEntityService<BLLAppDTO.BLLHoldingCompanyDTO>, IHoldingCompanyRepository<BLLAppDTO.BLLHoldingCompanyDTO>
    {
         Task<IEnumerable<BLLAppDTO.BLLHoldingCompanyDTO>> GetAllHoldingCompaniesAsync();
         Task<IEnumerable<BLLAppDTO.BLLHoldingCompanyDTO>> GetHoldingCompanySubsidiariesAsync(int id);
         Task<BLLAppDTO.BLLHoldingCompanyDTO> GetOneHoldingCompany(int id);
         Task<List<BLLAppDTO.BLLHoldingCompanyDTO>> GetHoldingCompaniesWithStatementSheets();

    }
}