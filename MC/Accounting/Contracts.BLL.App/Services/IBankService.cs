using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface IBankService : IBaseEntityService<BLLAppDTO.BLLBankDTO>, IBankRepository<BLLAppDTO.BLLBankDTO>
    {
        Task<IEnumerable<BLLAppDTO.BLLBankDTO>> GetCompanyBanksAsync(int id);
        Task<IEnumerable<BLLAppDTO.BLLBankDTO>> GetAllBanksAsync();
    }
}