using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Helpers;
using DAL.Base.EF.DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : BaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext dataContext, IBaseRepositoryProvider repositoryProvider) : base(dataContext, repositoryProvider)
        {
        }
        
        // no caching, every time new repo is created on access
        public IPersonRepository Persons =>
            _repositoryProvider.GetRepository<IPersonRepository>();
        public ICompanyPersonRepository CompanyPersons =>
            _repositoryProvider.GetRepository<ICompanyPersonRepository>();
        public IInvoiceRepository Invoices =>
            _repositoryProvider.GetRepository<IInvoiceRepository>();
        public IProjectRepository Projects  =>
            _repositoryProvider.GetRepository<IProjectRepository>();
        public IPersonsInvoiceRepository PersonsInvoices  =>
            _repositoryProvider.GetRepository<IPersonsInvoiceRepository>();
        public IDepartmentRepository Departments  =>
            _repositoryProvider.GetRepository<IDepartmentRepository>();
        public IInvoiceRowRepository InvoiceRows  =>
            _repositoryProvider.GetRepository<IInvoiceRowRepository>();
        public ISettingRepository Settings  =>
            _repositoryProvider.GetRepository<ISettingRepository>();
        public IWarehouseRepository Warehouses  =>
            _repositoryProvider.GetRepository<IWarehouseRepository>();
        public ICommodityRepository Commodities  =>
            _repositoryProvider.GetRepository<ICommodityRepository>();
        public IFiscalYearRepository FiscalYears  =>
            _repositoryProvider.GetRepository<IFiscalYearRepository>();
        public IStatementSheetRepository StatementSheets  =>
            _repositoryProvider.GetRepository<IStatementSheetRepository>();
        public IStatementReportRepository StatementReports  =>
            _repositoryProvider.GetRepository<IStatementReportRepository>();
        public ITransactionRepository Transactions  =>
            _repositoryProvider.GetRepository<ITransactionRepository>();
        public ICurrencyRepository Currencies  =>
            _repositoryProvider.GetRepository<ICurrencyRepository>();
        public IHoldingCompanyRepository HoldingCompanies =>
            _repositoryProvider.GetRepository<IHoldingCompanyRepository>();
        public IBankRepository Banks =>
            _repositoryProvider.GetRepository<IBankRepository>();
     
    }
}