using System;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;

namespace DAL.App.EF.Mappers
{
    public class StatementSheetMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.StatementSheetDTO))
            {
                return MapFromDomain((internalDTO.StatementSheet) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.StatementSheet))
            {
                return MapFromDAL((externalDTO.StatementSheetDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.StatementSheetDTO MapFromDomain(internalDTO.StatementSheet statementSheet)
        {
            var res = statementSheet == null ? null : new externalDTO.StatementSheetDTO()
            {
                Id = statementSheet.Id,
                Tally = statementSheet.Tally,
                TransactionCode = statementSheet.TransactionCode,
                HoldingCompanyId = statementSheet.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDomain(statementSheet.HoldingCompany),
            };

            return res;
        }
        
        public static internalDTO.StatementSheet MapFromDAL(externalDTO.StatementSheetDTO statementSheet)
        {
            var res = statementSheet == null ? null : new internalDTO.StatementSheet()
            {
                Id = statementSheet.Id,
                Tally = statementSheet.Tally,
                TransactionCode = statementSheet.TransactionCode,
                HoldingCompanyId = statementSheet.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDAL(statementSheet.HoldingCompany),
            };

            return res;
        }
    }
}