using System;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;

namespace DAL.App.EF.Mappers
{
    public class CommodityMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.CommodityDTO))
            {
                return MapFromDomain((internalDTO.Commodity) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Commodity))
            {
                return MapFromDAL((externalDTO.CommodityDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.CommodityDTO MapFromDomain(internalDTO.Commodity commodity)
        {
            var res = commodity == null ? null : new externalDTO.CommodityDTO()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDomain(commodity.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.Commodity MapFromDAL(externalDTO.CommodityDTO commodity)
        {
            var res = commodity == null ? null : new internalDTO.Commodity()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDAL(commodity.HoldingCompany)
            };

            return res;
        }
    }
}