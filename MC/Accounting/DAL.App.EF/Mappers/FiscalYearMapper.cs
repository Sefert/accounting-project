using System;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;


namespace DAL.App.EF.Mappers
{
    public class FiscalYearMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.FiscalYearDTO))
            {
                return MapFromDomain((internalDTO.FiscalYear) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.FiscalYear))
            {
                return MapFromDAL((externalDTO.FiscalYearDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.FiscalYearDTO MapFromDomain(internalDTO.FiscalYear fiscalYear)
        {
            var res = fiscalYear == null ? null : new externalDTO.FiscalYearDTO()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDomain(fiscalYear.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.FiscalYear MapFromDAL(externalDTO.FiscalYearDTO fiscalYear)
        {
            var res = fiscalYear == null ? null : new internalDTO.FiscalYear()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDAL(fiscalYear.HoldingCompany)
            };

            return res;
        }
    }
}