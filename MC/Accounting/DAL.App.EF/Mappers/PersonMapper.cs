using System;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;

namespace DAL.App.EF.Mappers
{
    public class PersonMapper  : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.PersonDTO))
            {
                return MapFromDomain((internalDTO.Person) inObject) as TOutObject;
            }
    
            if (typeof(TOutObject) == typeof(internalDTO.Person))
            {
                return MapFromDAL((externalDTO.PersonDTO) inObject) as TOutObject;
            }
    
            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }
    
    
        public static externalDTO.PersonDTO MapFromDomain(internalDTO.Person person)
        {
            var res = person == null ? null : new externalDTO.PersonDTO()
            {
                Id = person.Id,
                FirstName = person.FirstName,
                LastName = person.LastName,
                PhoneNumber = person.PhoneNumber,
                Email = person.Email,
                AppuserId = person.AppuserId,
            };
    
            return res;
        }
            
        public static internalDTO.Person MapFromDAL(externalDTO.PersonDTO person)
        {
            var res = person == null ? null : new internalDTO.Person()
            {
                Id = person.Id,
                FirstName = person.FirstName,
                LastName = person.LastName,
                PhoneNumber = person.PhoneNumber,
                Email = person.Email,
                AppuserId = person.AppuserId,
            };
    
            return res;
        }
    
    }
}