using System;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;

namespace DAL.App.EF.Mappers
{
    public class BankMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BankDTO))
            {
                return MapFromDomain((internalDTO.Bank) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Bank))
            {
                return MapFromDAL((externalDTO.BankDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BankDTO MapFromDomain(internalDTO.Bank bank)
        {
            var res = bank == null ? null : new externalDTO.BankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDomain(bank.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.Bank MapFromDAL(externalDTO.BankDTO bank)
        {
            var res = bank == null ? null : new internalDTO.Bank()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = HoldingCompanyMapper.MapFromDAL(bank.HoldingCompany)
            };

            return res;
        }
    }
}