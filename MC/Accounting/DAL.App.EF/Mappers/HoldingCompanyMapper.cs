using System;
using System.Linq;
using Contracts.DAL.Base.Mappers;
using internalDTO = Domain;
using externalDTO = DAL.App.DTO;

namespace DAL.App.EF.Mappers
{
    public class HoldingCompanyMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.HoldingCompanyDTO))
            {
                return MapFromDomain((internalDTO.HoldingCompany) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.HoldingCompany))
            {
                return MapFromDAL((externalDTO.HoldingCompanyDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.HoldingCompanyDTO MapFromDomain(internalDTO.HoldingCompany holdingCompany)
        {
            var res = holdingCompany == null ? null : new externalDTO.HoldingCompanyDTO()
            {
                Id = holdingCompany.Id,
                Name = holdingCompany.Name,
                Address = holdingCompany.Address,
                RegistrationNumber = holdingCompany.RegistrationNumber,
                VatRegistration = holdingCompany.VatRegistration,
                SubsidiaryId = holdingCompany.SubsidiaryId,
                //Subsidiary = HoldingCompanyMapper.MapFromDomain(holdingCompany.Subsidiary)
                //StatementSheets =  holdingCompany.StatementSheets?.Select(StatementSheetMapper.MapFromDomain).ToList()
            };

            return res;
        }
        
        public static internalDTO.HoldingCompany MapFromDAL(externalDTO.HoldingCompanyDTO holdingCompany)
        {
            var res = holdingCompany == null ? null : new internalDTO.HoldingCompany()
            {
                Id = holdingCompany.Id,
                Name = holdingCompany.Name,
                Address = holdingCompany.Address,
                RegistrationNumber = holdingCompany.RegistrationNumber,
                VatRegistration = holdingCompany.VatRegistration,
                SubsidiaryId = holdingCompany.SubsidiaryId,
                //Subsidiary = HoldingCompanyMapper.MapFromDAL(holdingCompany.Subsidiary)
                //StatementSheets =  holdingCompany.StatementSheets?.Select(StatementSheetMapper.MapFromDAL).ToList()
            };

            return res;
        }
    }
}