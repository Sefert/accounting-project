﻿using System.Linq;
using Contracts.DAL.Base;
using Domain;
using Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    //DATA ACCESS LIBRARY
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, int>, IDataContext
    {
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<CompanyPerson> CompanyPersons { get; set; }
        public DbSet<Department> Departments { get; set; }     
        public DbSet<FiscalYear> FiscalYears { get; set; }
        public DbSet<HoldingCompany> HoldingCompanies { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceRow> InvoiceRows { get; set; }
        public DbSet<Person> Persons { get; set; }       
        public DbSet<PersonsInvoice> PersonsInvoices { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Setting> Settings { get; set; }      
        public DbSet<StatementReport> StatementReports { get; set; }
        public DbSet<StatementSheet> StatementSheets { get; set; }       
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }      
        public DbSet<Currency> Currencies { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {     
        }
        protected override void OnModelCreating(ModelBuilder builder)
                {
                    base.OnModelCreating(builder);
        
                    // disable cascade delete
                    foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                    {
                        relationship.DeleteBehavior = DeleteBehavior.Restrict;
                    }
                }
    }
}