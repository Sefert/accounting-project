using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repository
{
    public class BankRepository : BaseRepository<BankDTO, Domain.Bank, AppDbContext>, IBankRepository
    {
        public BankRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new BankMapper())
        {
        }

        public async Task<List<BankDTO>> GetAllBanksAsync()
        {
            return await RepositoryDbSet
                //take a list of objects iterate over it and return list of smt else
                .Select(b => BankMapper.MapFromDomain(b))
                .ToListAsync();
        }
        
        public async Task<List<BankDTO>> GetCompanyBanksAsync(int id)
        {
            return await RepositoryDbSet
                 //take a list of objects iterate over it and return list of smt else
                .Where(b => b.HoldingCompanyId == id)
                .Select(b => BankMapper.MapFromDomain(b))
                .ToListAsync();
        }
    }
}