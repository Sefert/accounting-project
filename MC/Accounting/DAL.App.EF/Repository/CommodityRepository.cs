using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;


namespace DAL.App.EF.Repository
{
    public class CommodityRepository : BaseRepository<CommodityDTO, Domain.Commodity, AppDbContext>, ICommodityRepository
    {
        public CommodityRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new CommodityMapper())
        {
        }
        
        public async Task<List<CommodityDTO>> GetCompanyCommoditiesAsync(int id)
        {
            return await RepositoryDbSet
                .Where(c => c.HoldingCompanyId == id)
                .Select(b => CommodityMapper.MapFromDomain(b))
                .ToListAsync();
        }
    }
}