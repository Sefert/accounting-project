using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repository
{
    public class HoldingCompanyRepository : BaseRepository<HoldingCompanyDTO, Domain.HoldingCompany, AppDbContext>, IHoldingCompanyRepository
    {
        public HoldingCompanyRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new HoldingCompanyMapper())
        {
        }
        
        public async Task<IEnumerable<HoldingCompanyDTO>> GetAllHoldingCompaniesAsync()
        {
            return await RepositoryDbSet
                //take a list of objects iterate over it and return list of smt else
                .Where(hc => HoldingCompanyMapper.MapFromDomain(hc).SubsidiaryId == null)
                .Select(hc => HoldingCompanyMapper.MapFromDomain(hc))
                .ToListAsync();
        }

        public async Task<IEnumerable<HoldingCompanyDTO>> GetHoldingCompanySubsidiariesAsync(int id)
        {
            return await RepositoryDbSet
                .Where(hc => HoldingCompanyMapper.MapFromDomain(hc).SubsidiaryId == id)
                .Select(hc => HoldingCompanyMapper.MapFromDomain(hc.Subsidiary))
                .ToListAsync();
        }
        
        public async Task<List<HoldingCompanyDTO>> GetHoldingCompaniesWithStatementSheets()
        {
            return await RepositoryDbSet
                .Where(hc => HoldingCompanyMapper.MapFromDomain(hc).SubsidiaryId == null)
                //.Include(hc => HoldingCompanyMapper.MapFromDomain(hc))
                //.Include(hc => hc.StatementSheets)
                //   .Where(ss => ss.HoldingCompanyId == hc.Id)
                //   .Select(ss => StatementSheetMapper.MapFromDomain(ss)))
                .Include(hc => hc.StatementSheets)
                .Select(e => HoldingCompanyMapper.MapFromDomain(e))
                .ToListAsync();
        }
        
        public async Task<IEnumerable<HoldingCompanyDTO>>  GetHoldingCompanyWithSubsidiariesAsync()
        {
            return await RepositoryDbSet
                .Include(hc => hc.Subsidiary)
                .Select(e => HoldingCompanyMapper.MapFromDomain(e)).ToListAsync();
        }

        public async Task<HoldingCompanyDTO> GetOneHoldingCompany(int id)
        {           
            return HoldingCompanyMapper.MapFromDomain(await RepositoryDbSet.FirstOrDefaultAsync(hc => hc.Id == id));
        }      
    }
}