using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repository
{
    public class StatementSheetRepository : BaseRepository<StatementSheetDTO, Domain.StatementSheet, AppDbContext>, IStatementSheetRepository
    {
        public StatementSheetRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new StatementSheetMapper())
        {
            
        }
        
        public async Task<List<StatementSheetDTO>> GetAllStatements()
        {
            return await RepositoryDbSet
                .Select(ss => StatementSheetMapper.MapFromDomain(ss))
                .ToListAsync();
        }

        public async Task<List<StatementSheetDTO>> GetStatementReportByHoldingCompanyId(int id)
        {
            return await RepositoryDbSet
                .Where(ss => ss.HoldingCompanyId == id)
                .Select(ss => StatementSheetMapper.MapFromDomain(ss))
                .ToListAsync();
        }

    }
}