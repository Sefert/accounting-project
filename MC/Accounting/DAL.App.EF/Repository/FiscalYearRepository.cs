using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repository
{
    public class FiscalYearRepository : BaseRepository<FiscalYearDTO, Domain.FiscalYear, AppDbContext>, IFiscalYearRepository
    {
        public FiscalYearRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext, new FiscalYearMapper())
        {
            
        }
        public async Task<IEnumerable<FiscalYearDTO>> GetFiscalYearsByHoldingCompanyId(int id)
        {
            return await RepositoryDbSet
                .Where(fy => fy.HoldingCompanyId == id)
                .Select(fy => FiscalYearMapper.MapFromDomain(fy))
                .ToListAsync();
        }

    }
}