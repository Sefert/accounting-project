using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repository;
using DAL.Base.EF.Helpers;

namespace DAL.App.EF.Helpers
{
    public class AppRepositoryFactory : BaseRepositoryFactory<AppDbContext>
    {

        public AppRepositoryFactory()
        {
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            AddToCreationMethods<IHoldingCompanyRepository>(dataContext => new HoldingCompanyRepository(dataContext));
            AddToCreationMethods<IBankRepository>(dataContext => new BankRepository(dataContext));
            AddToCreationMethods<IStatementSheetRepository>(dataContext => new StatementSheetRepository(dataContext));
            AddToCreationMethods<IFiscalYearRepository>(dataContext => new FiscalYearRepository(dataContext));
            AddToCreationMethods<ICommodityRepository>(dataContext => new CommodityRepository(dataContext));
        }
    }
}