using System.ComponentModel.DataAnnotations;

namespace DAL.App.DTO
{
    public class StatementSheetDTO
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Tally { get; set; }
        
        [Required]
        [MaxLength(16)]
        public string TransactionCode { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompanyDTO HoldingCompany { get; set; }       
    }
}