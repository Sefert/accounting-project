using System;
using System.ComponentModel.DataAnnotations;


namespace DAL.App.DTO
{
    public class FiscalYearDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(80, MinimumLength = 3, ErrorMessage = "Must be between 3 and 80 character in length.")]
        public string ProjectName { get; set; }
        
        [Required]
        public DateTime Start { get; set; }
        
        public DateTime? End { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public DepartmentDTO Department { get; set; }
    }
}