using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.App.DTO
{
    public class StatementReportDTO
    {
        public int Id { get; set; }
        
        [Column(TypeName = "decimal(15,3)")]
        public decimal Total { get; set; }

        public int FiscalYearId { get; set; }
        public FiscalYearDTO FiscalYear { get; set; }
        
        public int? StatementSheetId { get; set; }
        public StatementSheetDTO StatementSheet { get; set; }      
    }
}