namespace DAL.App.DTO
{
    public class PersonsInvoiceDTO
    {
        public int Id { get; set; }
        
        public int InvoiceId { get; set; }
        public InvoiceDTO Invoice { get; set; }  
        
        public int PersonId { get; set; }
        public PersonDTO Person { get; set; }  
        
        public int? InvoiceRowId { get; set; }
        public InvoiceRowDTO InvoiceRow { get; set; } 
    }
}