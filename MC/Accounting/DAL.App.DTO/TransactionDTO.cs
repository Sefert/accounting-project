using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.App.DTO
{
    public class TransactionDTO
    {
        public int Id { get; set; }
        
        [Required]
        public DateTime TransactionDate { get; set; } 
        
        public int StatementReportId { get; set; } 
        public StatementReportDTO StatementReport { get; set; }
         
        public int? WarehouseId { get; set; }
        public WarehouseDTO Warehouse { get; set; } 
        
        public int? InvoiceRowId { get; set; }
        public InvoiceRowDTO InvoiceRow { get; set; } 
        
        public int? InvoiceId { get; set; }
        public InvoiceDTO Invoice{ get; set; } 
    }
}