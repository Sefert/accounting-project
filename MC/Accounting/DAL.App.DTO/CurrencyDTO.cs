using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain;

namespace DAL.App.DTO
{
    public class CurrencyDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(4, ErrorMessage = "Must be less then 100 letters")]
        public string DocumentCurrency { get; set; }
        
        [Required]
        [StringLength(4, ErrorMessage = "Must be less then 100 letters")]
        public string ToBaseCurrency { get; set; }
        
        [Column(TypeName = "decimal(7,4)")]
        public decimal CurrencyRate { get; set; }       
    }
}