using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.App.DTO
{
    public class InvoiceRowDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Must be between 3 and 100 character in length.")]
        public string ItemName { get; set; }
        
        [Column(TypeName = "decimal(14,3)")]
        public decimal NetValue { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string UnitType { get; set; }
        
        [Column(TypeName = "decimal(8,2)")]
        public decimal Amount { get; set; }
        
        [MaxLength(5)]
        public string DiscountPercent { get; set; }
        
        public int DebitId { get; set; }
        public StatementSheetDTO Debit { get; set; }
        
        public int CreditId { get; set; }
        public StatementSheetDTO Credit { get; set; }
        
        public int? ProjectId { get; set; }
        public ProjectDTO Project { get; set; }
        
        public int? InvoiceId { get; set; }
        public InvoiceDTO Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public CommodityDTO Commodity { get; set; }       
    }
}