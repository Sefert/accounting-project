using System;
using System.ComponentModel.DataAnnotations;
using Domain;

namespace DAL.App.DTO
{
    public class CompanyPersonDTO
    {
        public int Id { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        
        public int? PersonId { get; set; }
        public PersonDTO Person { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public DepartmentDTO Department { get; set; }
    }
}