using System.ComponentModel.DataAnnotations;

namespace DAL.App.DTO
{
    public class SettingDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string FileName { get; set; }
        
        [Required]
        [StringLength(30)]
        public string ContentType { get; set; }
        
        [Required]
        public string FilePath { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public HoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public DepartmentDTO Department { get; set; }
        
        public int? InvoiceId { get; set; }
        public InvoiceDTO Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public CommodityDTO Commodity { get; set; }
        
        public int? WarehouseId { get; set; }
        public WarehouseDTO Warehouse { get; set; }
        
        public int? StatementSheetId { get; set; }
        public StatementSheetDTO StatementSheet { get; set; }

    }
}