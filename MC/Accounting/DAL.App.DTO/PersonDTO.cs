using DAL.App.DTO.Identity;
using System.ComponentModel.DataAnnotations;


namespace DAL.App.DTO
{
    public class PersonDTO
    {
        public int Id { get; set; }
            
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 characters")]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Must be less then 50 characters")]
        public string LastName { get; set; }
        
        [StringLength(30, ErrorMessage = "Must be less then 30 characters")]
        public string PhoneNumber { get; set; }    
        
        [EmailAddress]
        [StringLength(30, ErrorMessage = "Must be less then 30 characters")]
        public string Email { get; set; }

        public int AppuserId { get; set; }
        public AppUser AppUser { get; set; }
        
        //public ICollection<PersonsInvoice> PersonsInvoices { get; set; }
        //public ICollection<CompanyPerson> CompanyPersons { get; set; }
    }
}