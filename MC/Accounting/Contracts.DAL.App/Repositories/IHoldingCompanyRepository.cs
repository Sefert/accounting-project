using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{    
    public interface IHoldingCompanyRepository : IHoldingCompanyRepository<DALAppDTO.HoldingCompanyDTO>
    {
        //Task<List<DALAppDTO.ContactTypeWithContactCounts>> GetAllWithContactCountAsync();
        Task<IEnumerable<DALAppDTO.HoldingCompanyDTO>> GetAllHoldingCompaniesAsync();
        Task<DALAppDTO.HoldingCompanyDTO> GetOneHoldingCompany(int id);
        Task<IEnumerable<DALAppDTO.HoldingCompanyDTO>> GetHoldingCompanySubsidiariesAsync(int id);
        Task<List<DALAppDTO.HoldingCompanyDTO>> GetHoldingCompaniesWithStatementSheets();
    }
    public interface IHoldingCompanyRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {        
        Task<IEnumerable<TDALEntity>> GetHoldingCompanyWithSubsidiariesAsync();
    }
}
