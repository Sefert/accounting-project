using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;


namespace Contracts.DAL.App.Repositories
{
    public interface ICommodityRepository : ICommodityRepository<DALAppDTO.CommodityDTO>
    {
        Task<List<DALAppDTO.CommodityDTO>> GetCompanyCommoditiesAsync(int id);
    }
    public interface ICommodityRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {        
    }
}