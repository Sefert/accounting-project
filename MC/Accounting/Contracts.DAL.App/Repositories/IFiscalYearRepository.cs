using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{    
    public interface IFiscalYearRepository : IFiscalYearRepository<DALAppDTO.FiscalYearDTO>
    {
        Task<IEnumerable<DALAppDTO.FiscalYearDTO>> GetFiscalYearsByHoldingCompanyId(int id);
    }
    public interface IFiscalYearRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {        
    }
}
