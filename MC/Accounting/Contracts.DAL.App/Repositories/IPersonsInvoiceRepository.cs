using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IPersonsInvoiceRepository  : IBaseRepositoryAsync<PersonsInvoice>
    {
        //Task<IEnumerable<PersonsInvoiceDTO>> GetAllPersonInvoicesAsync();
    }
}