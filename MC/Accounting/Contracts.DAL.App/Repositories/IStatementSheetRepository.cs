using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IStatementSheetRepository : IStatementSheetRepository<DALAppDTO.StatementSheetDTO>
    {
        Task<List<DALAppDTO.StatementSheetDTO>> GetAllStatements();
        Task<List<DALAppDTO.StatementSheetDTO>> GetStatementReportByHoldingCompanyId(int id);
    }
    public interface IStatementSheetRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {        

    }
}