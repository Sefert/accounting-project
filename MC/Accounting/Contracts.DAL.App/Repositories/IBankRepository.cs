using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;


namespace Contracts.DAL.App.Repositories
{
    public interface IBankRepository : IBankRepository<DALAppDTO.BankDTO>
        {
            Task<List<DALAppDTO.BankDTO>> GetCompanyBanksAsync(int id);
            Task<List<DALAppDTO.BankDTO>> GetAllBanksAsync();
        }
        public interface IBankRepository<TDALEntity> : IBaseRepository<TDALEntity>
            where TDALEntity : class, new()
        {        
        }
}