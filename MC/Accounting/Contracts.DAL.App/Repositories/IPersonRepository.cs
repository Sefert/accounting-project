using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;

namespace Contracts.DAL.App.Repositories
{
    public interface IPersonRepository : IBaseRepositoryAsync<Domain.Person>
    {
        //Task<IEnumerable<PersonDTO>> GetAllPersonsAsync();
    }
}