using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        IBankRepository Banks { get; }
        IHoldingCompanyRepository HoldingCompanies { get; }
        IPersonRepository Persons { get; }
        ICompanyPersonRepository CompanyPersons { get; }
        IInvoiceRepository Invoices { get; }
        IProjectRepository Projects { get; }
        IPersonsInvoiceRepository PersonsInvoices { get; }
        IDepartmentRepository Departments { get; }
        IInvoiceRowRepository InvoiceRows { get; }
        ISettingRepository Settings { get; }
        IWarehouseRepository Warehouses { get; }
        ICommodityRepository Commodities { get; }
        IFiscalYearRepository FiscalYears { get; }
        IStatementSheetRepository StatementSheets { get; }
        IStatementReportRepository StatementReports { get; }
        ITransactionRepository Transactions { get; }
        ICurrencyRepository Currencies { get; }
    }
}