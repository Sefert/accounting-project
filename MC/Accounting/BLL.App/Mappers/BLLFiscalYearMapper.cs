using System;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLFiscalYearMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BLLFiscalYearDTO))
            {
                return MapFromDAL((internalDTO.FiscalYearDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.FiscalYearDTO))
            {
                return MapFromBLL((externalDTO.BLLFiscalYearDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BLLFiscalYearDTO MapFromDAL(internalDTO.FiscalYearDTO fiscalYear)
        {
            var res = fiscalYear == null ? null : new externalDTO.BLLFiscalYearDTO()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromDAL(fiscalYear.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.FiscalYearDTO MapFromBLL(externalDTO.BLLFiscalYearDTO fiscalYear)
        {
            var res = fiscalYear == null ? null : new internalDTO.FiscalYearDTO()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromBLL(fiscalYear.HoldingCompany)
            };

            return res;
        }
    }
}