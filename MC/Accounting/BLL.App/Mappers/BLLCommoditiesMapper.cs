using System;
using Contracts.BLL.Base;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLCommoditiesMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BLLCommodityDTO))
            {
                return MapFromDAL((internalDTO.CommodityDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.CommodityDTO))
            {
                return MapFromBLL((externalDTO.BLLCommodityDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BLLCommodityDTO MapFromDAL(internalDTO.CommodityDTO commodity)
        {
            var res = commodity == null ? null : new externalDTO.BLLCommodityDTO()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromDAL(commodity.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.CommodityDTO MapFromBLL(externalDTO.BLLCommodityDTO commodity)
        {
            var res = commodity == null ? null : new internalDTO.CommodityDTO()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromBLL(commodity.HoldingCompany)
            };

            return res;
        }
    }
}