using System;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLPersonMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
                    where TOutObject : class
                {
                    if (typeof(TOutObject) == typeof(externalDTO.BLLPersonDTO))
                    {
                        return MapFromDAL((internalDTO.PersonDTO) inObject) as TOutObject;
                    }
            
                    if (typeof(TOutObject) == typeof(internalDTO.PersonDTO))
                    {
                        return MapFromBLL((externalDTO.BLLPersonDTO) inObject) as TOutObject;
                    }
            
                    throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
                }
            
            
                public static externalDTO.BLLPersonDTO MapFromDAL(internalDTO.PersonDTO person)
                {
                    var res = person == null ? null : new externalDTO.BLLPersonDTO()
                    {
                        Id = person.Id,
                        FirstName = person.FirstName,
                        LastName = person.LastName,
                        PhoneNumber = person.PhoneNumber,
                        Email = person.Email,
                        AppuserId = person.AppuserId,
                    };
            
                    return res;
                }
                    
                public static internalDTO.PersonDTO MapFromBLL(externalDTO.BLLPersonDTO person)
                {
                    var res = person == null ? null : new internalDTO.PersonDTO()
                    {
                        Id = person.Id,
                        FirstName = person.FirstName,
                        LastName = person.LastName,
                        PhoneNumber = person.PhoneNumber,
                        Email = person.Email,
                        AppuserId = person.AppuserId,
                    };
            
                    return res;
                }
    }
}