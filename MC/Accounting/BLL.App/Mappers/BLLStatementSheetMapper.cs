using System;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLStatementSheetMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BLLStatementSheetDTO))
            {
                return MapFromDAL((internalDTO.StatementSheetDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.StatementSheetDTO))
            {
                return MapFromBLL((externalDTO.BLLStatementSheetDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException(
                $"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BLLStatementSheetDTO MapFromDAL(internalDTO.StatementSheetDTO statementSheet)
        {
            var res = statementSheet == null
                ? null
                : new externalDTO.BLLStatementSheetDTO()
                {
                    Id = statementSheet.Id,
                    Tally = statementSheet.Tally,
                    TransactionCode = statementSheet.TransactionCode,
                    HoldingCompanyId = statementSheet.HoldingCompanyId,
                    HoldingCompany = BLLHoldingCompanyMapper.MapFromDAL(statementSheet.HoldingCompany),
                };

            return res;
        }

        public static internalDTO.StatementSheetDTO MapFromBLL(externalDTO.BLLStatementSheetDTO statementSheet)
        {
            var res = statementSheet == null
                ? null
                : new internalDTO.StatementSheetDTO()
                {
                    Id = statementSheet.Id,
                    Tally = statementSheet.Tally,
                    TransactionCode = statementSheet.TransactionCode,
                    HoldingCompanyId = statementSheet.HoldingCompanyId,
                    HoldingCompany = BLLHoldingCompanyMapper.MapFromBLL(statementSheet.HoldingCompany),
                };

            return res;
        }
    }
}