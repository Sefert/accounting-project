using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLHoldingCompanyMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BLLHoldingCompanyDTO))
            {
                return MapFromDAL((internalDTO.HoldingCompanyDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.HoldingCompanyDTO))
            {
                return MapFromBLL((externalDTO.BLLHoldingCompanyDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BLLHoldingCompanyDTO MapFromDAL(internalDTO.HoldingCompanyDTO holdingCompany)
        {
            var res = holdingCompany == null ? null : new externalDTO.BLLHoldingCompanyDTO()
            {
                Id = holdingCompany.Id,
                Name = holdingCompany.Name,
                Address = holdingCompany.Address,
                RegistrationNumber = holdingCompany.RegistrationNumber,
                VatRegistration = holdingCompany.VatRegistration,
                SubsidiaryId = holdingCompany.SubsidiaryId,
                //Subsidiary = MapFromDAL(holdingCompany.Subsidiary)
                //StatementSheets =  holdingCompany.StatementSheets?.Select(ss => BLLStatementSheetMapper.MapFromDAL(ss)).ToList()
            };

            return res;
        }
        
        public static internalDTO.HoldingCompanyDTO MapFromBLL(externalDTO.BLLHoldingCompanyDTO holdingCompany)
        {
            var res = holdingCompany == null ? null : new internalDTO.HoldingCompanyDTO()
            {
                Id = holdingCompany.Id,
                Name = holdingCompany.Name,
                Address = holdingCompany.Address,
                RegistrationNumber = holdingCompany.RegistrationNumber,
                VatRegistration = holdingCompany.VatRegistration,
                SubsidiaryId = holdingCompany.SubsidiaryId,
                //Subsidiary = MapFromBLL(holdingCompany.Subsidiary)
                //StatementSheets =  holdingCompany.StatementSheets?.Select(ss => BLLStatementSheetMapper.MapFromBLL(ss)).ToList()
            };

            return res;
        }
    }
}