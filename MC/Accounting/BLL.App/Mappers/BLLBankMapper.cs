using System;
using Contracts.BLL.Base.Mappers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BLLBankMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BLLBankDTO))
            {
                return MapFromDAL((internalDTO.BankDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BankDTO))
            {
                return MapFromBLL((externalDTO.BLLBankDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.BLLBankDTO MapFromDAL(internalDTO.BankDTO bank)
        {
            var res = bank == null ? null : new externalDTO.BLLBankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromDAL(bank.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.BankDTO MapFromBLL(externalDTO.BLLBankDTO bank)
        {
            var res = bank == null ? null : new internalDTO.BankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = BLLHoldingCompanyMapper.MapFromBLL(bank.HoldingCompany)
            };

            return res;
        }
    }
}