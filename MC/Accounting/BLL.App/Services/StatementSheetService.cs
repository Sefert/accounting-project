using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class StatementSheetService  : BaseEntityService<BLL.App.DTO.BLLStatementSheetDTO, DAL.App.DTO.StatementSheetDTO, IAppUnitOfWork>, IStatementSheetService
    {
        public StatementSheetService(IAppUnitOfWork uow) : base(uow, new BLLStatementSheetMapper())
        {
            ServiceRepository = Uow.StatementSheets;
        }

        public async Task<IEnumerable<BLLStatementSheetDTO>> GetAllStatementsAsync()
        {
            return (await Uow.StatementSheets.GetAllStatements()).Select(e => BLLStatementSheetMapper.MapFromDAL(e));
        }
        
        public async Task<IEnumerable<BLLStatementSheetDTO>> GetStatementReportByHoldingCompanyId(int id)
        {
            return (await Uow.StatementSheets.GetStatementReportByHoldingCompanyId(id)).Select(e => BLLStatementSheetMapper.MapFromDAL(e));
        }
    }
}