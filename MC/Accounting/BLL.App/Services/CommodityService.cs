using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class CommodityService : BaseEntityService<BLLCommodityDTO, DAL.App.DTO.CommodityDTO, IAppUnitOfWork>, ICommodityService
    {
        public CommodityService(IAppUnitOfWork uow) : base(uow, new BLLCommoditiesMapper())
        {
            ServiceRepository = Uow.Commodities;
        }
        
        public async Task<IEnumerable<BLLCommodityDTO>> GetCompanyCommoditiesAsync(int id)
        {
            return (await Uow.Commodities.GetCompanyCommoditiesAsync(id)).Select(c => BLLCommoditiesMapper.MapFromDAL(c));
        }
        
    }
}