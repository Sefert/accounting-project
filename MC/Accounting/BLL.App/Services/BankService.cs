using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class BankService : BaseEntityService<BLLBankDTO, DAL.App.DTO.BankDTO, IAppUnitOfWork>, IBankService
    {
        public BankService(IAppUnitOfWork uow) : base(uow, new BLLBankMapper())
        {
            ServiceRepository = Uow.Banks;
        }

        public async Task<IEnumerable<BLLBankDTO>> GetCompanyBanksAsync(int id)
        {
            return (await Uow.Banks.GetCompanyBanksAsync(id)).Select(b => BLLBankMapper.MapFromDAL(b));
        }

        public async Task<IEnumerable<BLLBankDTO>> GetAllBanksAsync()
        {
            return (await Uow.Banks.GetAllBanksAsync()).Select(b => BLLBankMapper.MapFromDAL(b));
        }
    }
}