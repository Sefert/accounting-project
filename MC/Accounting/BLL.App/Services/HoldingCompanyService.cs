using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class HoldingCompanyService  : BaseEntityService<BLLHoldingCompanyDTO, DAL.App.DTO.HoldingCompanyDTO, IAppUnitOfWork>, IHoldingCompanyService
    {
        public HoldingCompanyService(IAppUnitOfWork uow) : base(uow, new BLLHoldingCompanyMapper())
        {
            ServiceRepository = Uow.HoldingCompanies;
        }

        public async Task<IEnumerable<BLLHoldingCompanyDTO>> GetAllHoldingCompaniesAsync()
        {
            return (await Uow.HoldingCompanies.GetAllHoldingCompaniesAsync()).Select(e => BLLHoldingCompanyMapper.MapFromDAL(e));
        }

        public async Task<IEnumerable<BLLHoldingCompanyDTO>> GetHoldingCompanyWithSubsidiariesAsync()
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<BLLHoldingCompanyDTO>> GetHoldingCompanySubsidiariesAsync(int id)
        {
            return (await Uow.HoldingCompanies.GetHoldingCompanySubsidiariesAsync(id)).Select(e => BLLHoldingCompanyMapper.MapFromDAL(e));
        }

        public async Task<BLLHoldingCompanyDTO> GetOneHoldingCompany(int id)
        {
            return BLLHoldingCompanyMapper.MapFromDAL(await Uow.HoldingCompanies.GetOneHoldingCompany(id));
        }        
        public async Task<List<BLLHoldingCompanyDTO>> GetHoldingCompaniesWithStatementSheets()
        {
            return (await Uow.HoldingCompanies.GetHoldingCompaniesWithStatementSheets()).Select(e => BLLHoldingCompanyMapper.MapFromDAL(e)).ToList();           
        }
        
    }

}