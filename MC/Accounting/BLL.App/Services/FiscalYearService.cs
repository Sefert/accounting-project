using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class FiscalYearService : BaseEntityService<BLL.App.DTO.BLLFiscalYearDTO, DAL.App.DTO.FiscalYearDTO, IAppUnitOfWork>, IFiscalYearService
    {
        public FiscalYearService(IAppUnitOfWork uow) : base(uow, new BLLFiscalYearMapper())
        {
            ServiceRepository = Uow.FiscalYears;
        }

        public async Task<IEnumerable<BLLFiscalYearDTO>> GetFiscalYearsByHoldingCompanyId(int id)
        {
            return (await Uow.FiscalYears.GetFiscalYearsByHoldingCompanyId(id)).Select(e => BLLFiscalYearMapper.MapFromDAL(e));
        }
    }
}