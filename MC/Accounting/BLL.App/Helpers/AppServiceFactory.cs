using BLL.App.Services;
using BLL.Base.Helpers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Helpers
{
    public class AppServiceFactory : BaseServiceFactory<IAppUnitOfWork>
    {
        public AppServiceFactory()
        {
            RegisterServices();
        }

        private void RegisterServices()
        {
            // Register all your custom services here!
            AddToCreationMethods<IHoldingCompanyService>(uow => new HoldingCompanyService(uow));            
            AddToCreationMethods<IBankService>(uow => new BankService(uow));
            AddToCreationMethods<IStatementSheetService>(uow => new StatementSheetService(uow));
            AddToCreationMethods<IFiscalYearService>(uow => new FiscalYearService(uow));
            AddToCreationMethods<ICommodityService>(uow => new CommodityService(uow));
        }

    }
    
}