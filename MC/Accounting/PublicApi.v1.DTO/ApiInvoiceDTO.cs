using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PublicApi.v1.DTO
{
    public class ApiInvoiceDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string InvoiceNumber { get; set; }
        
        [Required]
        public DateTime InvoiceDate { get; set; }
        
        [Required]
        public DateTime DueDate { get; set; }
        
        [Column(TypeName = "decimal(11,3)")]
        public decimal TotalAmount { get; set; }
        
        [Column(TypeName = "decimal(5,2)")]
        public decimal TaxationPercent { get; set; }
        
        [Column(TypeName = "decimal(5,2)")]
        public decimal DiscountPercent { get; set; }
        
        [Required]
        public Boolean TransactionConfirmed { get; set; }
        
        [Required]
        public string InvoicePath { get; set; }
        
        [Required]
        public string FileName { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public ApiDepartmentDTO Department { get; set; }
        
        public int? ProjectId { get; set; }
        public ApiProjectDTO Project { get; set; }
        
        public int? CurrencyId { get; set; }
        public ApiCurrencyDTO Currency { get; set; }
    }
}