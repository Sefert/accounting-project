using System.ComponentModel.DataAnnotations.Schema;

namespace PublicApi.v1.DTO
{
    public class ApiStatementReportDTO
    {
        public int Id { get; set; }
        
        [Column(TypeName = "decimal(15,3)")]
        public decimal Total { get; set; }

        public int FiscalYearId { get; set; }
        public ApiFiscalYearDTO FiscalYear { get; set; }
        
        public int? StatementSheetId { get; set; }
        public ApiStatementSheetDTO StatementSheet { get; set; }
    }
}