using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PublicApi.v1.DTO
{
    public class ApiCurrencyDTO
    {
        public int Id { get; set; }
                        
        [Required]
        [StringLength(4, ErrorMessage = "Must be less then 100 letters")]
        public string DocumentCurrency { get; set; }
                        
        [Required]
        [StringLength(4, ErrorMessage = "Must be less then 100 letters")]
        public string ToBaseCurrency { get; set; }
                        
        [Column(TypeName = "decimal(7,4)")]
        public decimal CurrencyRate { get; set; }    
    }
}