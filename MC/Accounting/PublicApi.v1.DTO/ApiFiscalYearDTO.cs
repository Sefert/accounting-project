using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiFiscalYearDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(80, MinimumLength = 3, ErrorMessage = "Must be between 3 and 80 character in length.")]
        public string ProjectName { get; set; }
        
        [Required]
        public DateTime Start { get; set; }
        
        public DateTime? End { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public ApiDepartmentDTO Department { get; set; }        
    }
}