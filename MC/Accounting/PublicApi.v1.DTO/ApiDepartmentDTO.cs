using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiDepartmentDTO
    {
        public int Id { get; set; }
                        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Name { get; set; }
                        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Address { get; set; }
                        
        public int HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; } 
    }
}