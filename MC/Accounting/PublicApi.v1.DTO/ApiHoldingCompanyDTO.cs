using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiHoldingCompanyDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Address { get; set; }
        
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string RegistrationNumber { get; set; }
       
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string VatRegistration { get; set; }       
        
        public int? SubsidiaryId { get; set; }
        //public ApiHoldingCompanyDTO Subsidiary { get; set; } 
        
        //public ICollection<ApiStatementSheetDTO> StatementSheets { get; set; }
    }
}