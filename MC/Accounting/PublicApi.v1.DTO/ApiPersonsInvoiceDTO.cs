namespace PublicApi.v1.DTO
{
    public class ApiPersonsInvoiceDTO
    {
        public int Id { get; set; }
        
        public int InvoiceId { get; set; }
        public ApiInvoiceDTO Invoice { get; set; }  
        
        public int PersonId { get; set; }
        public ApiPersonDTO Person { get; set; }  
        
        public int? InvoiceRowId { get; set; }
        public ApiInvoiceRowDTO InvoiceRow { get; set; } 
    }
}