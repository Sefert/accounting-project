using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiSettingDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string FileName { get; set; }
        
        [Required]
        [StringLength(30)]
        public string ContentType { get; set; }
        
        [Required]
        public string FilePath { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public ApiDepartmentDTO Department { get; set; }
        
        public int? InvoiceId { get; set; }
        public ApiInvoiceDTO Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public ApiCommodityDTO Commodity { get; set; }
        
        public int? WarehouseId { get; set; }
        public ApiWarehouseDTO Warehouse { get; set; }
        
        public int? StatementSheetId { get; set; }
        public ApiStatementSheetDTO StatementSheet { get; set; }
    }
}