using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiTransactionDTO
    {
        public int Id { get; set; }
        
        [Required]
        public DateTime TransactionDate { get; set; } 
        
        public int StatementReportId { get; set; } 
        public ApiStatementReportDTO StatementReport { get; set; }
         
        public int? WarehouseId { get; set; }
        public ApiWarehouseDTO Warehouse { get; set; } 
        
        public int? InvoiceRowId { get; set; }
        public ApiInvoiceRowDTO InvoiceRow { get; set; } 
        
        public int? InvoiceId { get; set; }
        public ApiInvoiceDTO Invoice{ get; set; } 
    }
}