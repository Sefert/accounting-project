using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiStatementSheetDTO
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Tally { get; set; }
        
        [Required]
        [MaxLength(16)]
        public string TransactionCode { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; }   
    }
}