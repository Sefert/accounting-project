using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class ApiBankDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, ErrorMessage = "Must be less then 255 characters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Must be less then 50 characters")]
        public string Account { get; set; }     
        
        public string Iban { get; set; } 
        
        public string SwiftBic { get; set; } 
        
        public int HoldingCompanyId { get; set; }
        public ApiHoldingCompanyDTO HoldingCompany { get; set; }
    }
}