using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLCompanyPersonDTO
    {
        public int Id { get; set; }
        
        [Required]
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        
        public int? PersonId { get; set; }
        public BLLPersonDTO Person { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public BLLDepartmentDTO Department { get; set; }
    }
}