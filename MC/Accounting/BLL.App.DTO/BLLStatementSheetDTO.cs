using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLStatementSheetDTO
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Tally { get; set; }
        
        [Required]
        [MaxLength(16)]
        public string TransactionCode { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }   
    }
}