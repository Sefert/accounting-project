using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLSettingDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string FileName { get; set; }
        
        [Required]
        [StringLength(30)]
        public string ContentType { get; set; }
        
        [Required]
        public string FilePath { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public BLLDepartmentDTO Department { get; set; }
        
        public int? InvoiceId { get; set; }
        public BLLInvoiceDTO Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public BLLCommodityDTO Commodity { get; set; }
        
        public int? WarehouseId { get; set; }
        public BLLWarehouseDTO Warehouse { get; set; }
        
        public int? StatementSheetId { get; set; }
        public BLLStatementSheetDTO StatementSheet { get; set; }
    }
}