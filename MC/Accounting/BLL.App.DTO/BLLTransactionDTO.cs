using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLTransactionDTO
    {
        public int Id { get; set; }
        
        [Required]
        public DateTime TransactionDate { get; set; } 
        
        public int StatementReportId { get; set; } 
        public BLLStatementReportDTO StatementReport { get; set; }
         
        public int? WarehouseId { get; set; }
        public BLLWarehouseDTO Warehouse { get; set; } 
        
        public int? InvoiceRowId { get; set; }
        public BLLInvoiceRowDTO InvoiceRow { get; set; } 
        
        public int? InvoiceId { get; set; }
        public BLLInvoiceDTO Invoice{ get; set; } 
    }
}