using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLBankDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255, ErrorMessage = "Must be less then 255 characters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Must be less then 50 characters")]
        public string Account { get; set; }     
        
        public string Iban { get; set; } 
        
        public string SwiftBic { get; set; } 
        
        public int HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }
    }
}