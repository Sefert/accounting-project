using System.ComponentModel.DataAnnotations.Schema;

namespace BLL.App.DTO
{
    public class BLLStatementReportDTO
    {
        public int Id { get; set; }
        
        [Column(TypeName = "decimal(15,3)")]
        public decimal Total { get; set; }

        public int FiscalYearId { get; set; }
        public BLLFiscalYearDTO FiscalYear { get; set; }
        
        public int? StatementSheetId { get; set; }
        public BLLStatementSheetDTO StatementSheet { get; set; }
    }
}