using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace BLL.App.DTO
{
    public class BLLHoldingCompanyDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Address { get; set; }
        
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string RegistrationNumber { get; set; }
       
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string VatRegistration { get; set; }       
        
        public int? SubsidiaryId { get; set; }
        //public BLLHoldingCompanyDTO Subsidiary { get; set; } 
        
        //public ICollection<BLLStatementSheetDTO> StatementSheets { get; set; }
    }
}