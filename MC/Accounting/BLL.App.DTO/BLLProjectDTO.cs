using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BLLProjectDTO
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Must be between 3 and 100 character in length.")]
        public string ProjectName { get; set; }
        
        [Required]
        public DateTime Start { get; set; }
        
        public DateTime? End { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public BLLDepartmentDTO Department { get; set; }
    }
}