using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BLL.App.DTO
{
    public class BLLCommodityDTO
    {
        public int Id { get; set; }
                
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Must be between 3 and 100 character in length.")]
        public string ItemName { get; set; }
                
        [Column(TypeName = "decimal(14,3)")]
        public decimal NetValue { get; set; }
                
        [Required]
        [MaxLength(20)]
        public string UnitType { get; set; }
                
        public int? HoldingCompanyId { get; set; }
        public BLLHoldingCompanyDTO HoldingCompany { get; set; }
                
        public int? DepartmentId { get; set; }
        public BLLDepartmentDTO Department { get; set; }
                
        //TODO: fix this
        //public int? CommoditiesId { get; set; }
        //public Commodity Commodities { get; set; }    
    }
}