namespace BLL.App.DTO
{
    public class BLLPersonsInvoiceDTO
    {
        public int Id { get; set; }
        
        public int InvoiceId { get; set; }
        public BLLInvoiceDTO Invoice { get; set; }  
        
        public int PersonId { get; set; }
        public BLLPersonDTO Person { get; set; }  
        
        public int? InvoiceRowId { get; set; }
        public BLLInvoiceRowDTO InvoiceRow { get; set; } 
    }
}