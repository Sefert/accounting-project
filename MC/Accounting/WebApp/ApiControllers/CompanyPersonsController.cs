using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyPersonsController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public CompanyPersonsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/CompanyPersons
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CompanyPerson>>> GetCompanyPersons()
        {
            return Ok(await _uow.CompanyPersons.AllAsync());
        }

        // GET: api/CompanyPersons/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CompanyPerson>> GetCompanyPerson(int id)
        {
            var companyPerson = await _uow.CompanyPersons.FindAsync(id);

            if (companyPerson == null)
            {
                return NotFound();
            }

            return companyPerson;
        }

        // PUT: api/CompanyPersons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompanyPerson(int id, CompanyPerson companyPerson)
        {
            if (id != companyPerson.Id)
            {
                return BadRequest();
            }

            _uow.CompanyPersons.Update(companyPerson);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/CompanyPersons
        [HttpPost]
        public async Task<ActionResult<CompanyPerson>> PostCompanyPerson(CompanyPerson companyPerson)
        {
            await _uow.CompanyPersons.AddAsync(companyPerson);
            await _uow.SaveChangesAsync();

            return CreatedAtAction("GetCompanyPerson", new { id = companyPerson.Id }, companyPerson);
        }

        // DELETE: api/CompanyPersons/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CompanyPerson>> DeleteCompanyPerson(int id)
        {
            var companyPerson = await _uow.CompanyPersons.FindAsync(id);
            if (companyPerson == null)
            {
                return NotFound();
            }

            _uow.CompanyPersons.Remove(companyPerson);
            await _uow.SaveChangesAsync();

            return companyPerson;
        }
    }
}
