using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]   
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FiscalYearsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        public FiscalYearsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/FiscalYears
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiFiscalYearDTO>>> GetFiscalYears()
        {
            return (await _bll.FiscalYears.AllAsync())
                .Select(e => PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(e)).ToList();
        }

        // GET: api/FiscalYears/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiFiscalYearDTO>> GetFiscalYear(int id)
        {
            var fiscalYear =  PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(await _bll.FiscalYears.FindAsync(id));

            if (fiscalYear == null)
            {
                return NotFound();
            }

            return fiscalYear;
        }

        // PUT: api/FiscalYears/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFiscalYear(int id, PublicApi.v1.DTO.ApiFiscalYearDTO fiscalYear)
        {
            if (id != fiscalYear.Id)
            {
                return BadRequest();
            }

            _bll.FiscalYears.Update(PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromExternal(fiscalYear));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/FiscalYears
        [HttpPost]
        public async Task<ActionResult<FiscalYear>> PostFiscalYear(PublicApi.v1.DTO.ApiFiscalYearDTO fiscalYear)
        {
            fiscalYear = PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(
                _bll.FiscalYears.Add(PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromExternal(fiscalYear)));
            await _bll.SaveChangesAsync();
            fiscalYear = PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(
                _bll.FiscalYears.GetUpdatesAfterUOWSaveChanges(PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromExternal(fiscalYear)));

            return CreatedAtAction("GetFiscalYear", new { version = HttpContext.GetRequestedApiVersion().ToString(), id = fiscalYear.Id }, fiscalYear);
        }

        // DELETE: api/FiscalYears/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiFiscalYearDTO>> DeleteFiscalYear(int id)
        {
            var fiscalYear = PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(await _bll.FiscalYears.FindAsync(id));
            if (fiscalYear == null)
            {
                return NotFound();
            }

            _bll.FiscalYears.Remove(fiscalYear.Id);
            await _bll.SaveChangesAsync();

            return fiscalYear;
        }
    }
}
