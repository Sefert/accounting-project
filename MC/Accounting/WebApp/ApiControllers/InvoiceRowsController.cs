using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceRowsController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public InvoiceRowsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/InvoiceRows
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InvoiceRow>>> GetInvoiceRows()
        {
            return Ok(await _uow.InvoiceRows.AllAsync());
        }

        // GET: api/InvoiceRows/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InvoiceRow>> GetInvoiceRow(int id)
        {
            var invoiceRow = await _uow.InvoiceRows.FindAsync(id);

            if (invoiceRow == null)
            {
                return NotFound();
            }

            return invoiceRow;
        }

        // PUT: api/InvoiceRows/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInvoiceRow(int id, InvoiceRow invoiceRow)
        {
            if (id != invoiceRow.Id)
            {
                return BadRequest();
            }

            _uow.InvoiceRows.Update(invoiceRow);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/InvoiceRows
        [HttpPost]
        public async Task<ActionResult<InvoiceRow>> PostInvoiceRow(InvoiceRow invoiceRow)
        {
            await _uow.InvoiceRows.AddAsync(invoiceRow);
            await _uow.SaveChangesAsync();

            return CreatedAtAction("GetInvoiceRow", new { id = invoiceRow.Id }, invoiceRow);
        }

        // DELETE: api/InvoiceRows/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<InvoiceRow>> DeleteInvoiceRow(int id)
        {
            var invoiceRow = await _uow.InvoiceRows.FindAsync(id);
            if (invoiceRow == null)
            {
                return NotFound();
            }

            _uow.InvoiceRows.Remove(invoiceRow);
            await _uow.SaveChangesAsync();

            return invoiceRow;
        }
    }
}
