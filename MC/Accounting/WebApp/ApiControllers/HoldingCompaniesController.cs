using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]   
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class HoldingCompaniesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        public HoldingCompaniesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/HoldingCompanies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiHoldingCompanyDTO>>> GetHoldingCompanies()
        {
            return (await _bll.HoldingCompanies.GetAllHoldingCompaniesAsync())
                .Select(e => PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(e)).ToList();                
        }
        
         //GET: api/HoldingCompanies/Subsidiaries
        [HttpGet("Subsidiaries/{id}")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiHoldingCompanyDTO>>> GetHoldingCompanySubsidiaries(int id)
        {
            return (await _bll.HoldingCompanies.GetHoldingCompanySubsidiariesAsync(id))
                .Select(e => PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(e)).ToList(); 
        }
        
        // GET: api/HoldingCompanies/5/StatementSheets
        [HttpGet("{id}/StatementSheets")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiStatementSheetDTO>>> GetStatementReportByHoldingCompanyId(int id)
        {
            var statementSheets = (await _bll.Statements.GetStatementReportByHoldingCompanyId(id))
                .Select(e => PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(e)).ToList();

            return statementSheets;
        }
         
        // GET: api/HoldingCompanies/5/Banks
        [HttpGet("{id}/Banks")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiBankDTO>>> GetBankByHoldingCompanyId(int id)
        {
            var banks = (await _bll.Banks.GetCompanyBanksAsync(id))
                .Select(e => PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(e)).ToList();

            return banks;
        }
        
        // GET: api/HoldingCompanies/5/FiscalYears
        [HttpGet("{id}/FiscalYears")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiFiscalYearDTO>>> GetFiscalYearByHoldingCompanyId(int id)
        {
            var fiscalYears = (await _bll.FiscalYears.GetFiscalYearsByHoldingCompanyId(id))
                .Select(e => PublicApi.v1.Mappers.ApiFiscalYearMapper.MapFromBLL(e)).ToList();

            return fiscalYears;
        }
        
        // GET: api/HoldingCompanies/5/Commodities
        [HttpGet("{id}/Commodities")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiCommodityDTO>>> GetCommoditiesByHoldingCompanyId(int id)
        {
            var commodities = (await _bll.Commodities.GetCompanyCommoditiesAsync(id))
                .Select(e => PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(e)).ToList();

            return commodities;
        }
        
        
        //GET: api/HoldingCompanies/StatementSheets
        /*[HttpGet("StatementSheets")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiHoldingCompanyDTO>>> GetHoldingCompanyWithStatmentSheets()
        {
            return (await _bll.HoldingCompanies.GetHoldingCompaniesWithStatementSheets())
                .Select(e => PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(e)).ToList();        
        }*/

        // GET: api/HoldingCompanies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiHoldingCompanyDTO>> GetHoldingCompany(int id)
        {
            var holdingCompany = PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(await _bll.HoldingCompanies.GetOneHoldingCompany(id));

            if (holdingCompany == null)
            {
                return NotFound();
            }

            return holdingCompany;
        }

        // PUT: api/HoldingCompanies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHoldingCompany(int id, PublicApi.v1.DTO.ApiHoldingCompanyDTO holdingCompany)
        {
            
            if (id != holdingCompany.Id)
            {
                return BadRequest();
            }

            _bll.HoldingCompanies.Update(PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromExternal(holdingCompany));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/HoldingCompanies
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiHoldingCompanyDTO>> PostHoldingCompany(PublicApi.v1.DTO.ApiHoldingCompanyDTO holdingCompany)
        {
            holdingCompany = PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(
                _bll.HoldingCompanies.Add(PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromExternal(holdingCompany)));
            //_bll.HoldingCompanies.Add(PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromExternal(holdingCompany));
            await _bll.SaveChangesAsync();
            holdingCompany = PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(
                _bll.HoldingCompanies.GetUpdatesAfterUOWSaveChanges(PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromExternal(holdingCompany)));
            return CreatedAtAction("GetHoldingCompany", new
            {
                version = HttpContext.GetRequestedApiVersion().ToString(),
                id = holdingCompany.Id
            }, holdingCompany);
        }

        // DELETE: api/HoldingCompanies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiHoldingCompanyDTO>> DeleteHoldingCompany(int id)
        {
            //var holdingCompany = await _bll.HoldingCompanies.FindAsync(id);
            var holdingCompany = PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromBLL(await _bll.HoldingCompanies.FindAsync(id));
            if (holdingCompany == null)
            {
                return NotFound();
            }

            _bll.HoldingCompanies.Remove(holdingCompany.Id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}
