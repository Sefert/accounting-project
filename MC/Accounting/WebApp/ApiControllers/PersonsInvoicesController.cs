using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsInvoicesController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public PersonsInvoicesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/PersonsInvoices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonsInvoice>>> GetPersonsInvoices()
        {
            return Ok(await _uow.PersonsInvoices.AllAsync());
        }

        // GET: api/PersonsInvoices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonsInvoice>> GetPersonsInvoice(int id)
        {
            var personsInvoice = await _uow.PersonsInvoices.FindAsync(id);

            if (personsInvoice == null)
            {
                return NotFound();
            }

            return personsInvoice;
        }

        // PUT: api/PersonsInvoices/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonsInvoice(int id, PersonsInvoice personsInvoice)
        {
            if (id != personsInvoice.Id)
            {
                return BadRequest();
            }

            _uow.PersonsInvoices.Update(personsInvoice);
            await _uow.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/PersonsInvoices
        [HttpPost]
        public async Task<ActionResult<PersonsInvoice>> PostPersonsInvoice(PersonsInvoice personsInvoice)
        {
            await _uow.PersonsInvoices.AddAsync(personsInvoice);
            await _uow.SaveChangesAsync();

            return CreatedAtAction("GetPersonsInvoice", new { id = personsInvoice.Id }, personsInvoice);
        }

        // DELETE: api/PersonsInvoices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PersonsInvoice>> DeletePersonsInvoice(int id)
        {
            var personsInvoice = await _uow.PersonsInvoices.FindAsync(id);
            if (personsInvoice == null)
            {
                return NotFound();
            }

            _uow.PersonsInvoices.Remove(personsInvoice);
            await _uow.SaveChangesAsync();

            return personsInvoice;
        }
    }
}
