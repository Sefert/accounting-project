using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StatementReportsController : ControllerBase
    {
        private readonly IAppUnitOfWork _uow;

        public StatementReportsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: api/StatementReports
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatementReport>>> GetStatementReports()
        {
            return Ok(await _uow.StatementReports.AllAsync());
        }

        // GET: api/StatementReports/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StatementReport>> GetStatementReport(int id)
        {
            var statementReport = await _uow.StatementReports.FindAsync(id);

            if (statementReport == null)
            {
                return NotFound();
            }

            return statementReport;
        }

        // PUT: api/StatementReports/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatementReport(int id, StatementReport statementReport)
        {
            if (id != statementReport.Id)
            {
                return BadRequest();
            }

            _uow.StatementReports.Update(statementReport);
            await _uow.SaveChangesAsync();
           
            return NoContent();
        }

        // POST: api/StatementReports
        [HttpPost]
        public async Task<ActionResult<StatementReport>> PostStatementReport(StatementReport statementReport)
        {
            await _uow.StatementReports.AddAsync(statementReport);
            await _uow.SaveChangesAsync();

            return CreatedAtAction("GetStatementReport", new { id = statementReport.Id }, statementReport);
        }

        // DELETE: api/StatementReports/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StatementReport>> DeleteStatementReport(int id)
        {
            var statementReport = await _uow.StatementReports.FindAsync(id);
            if (statementReport == null)
            {
                return NotFound();
            }

            return statementReport;
        }
    }
}
