using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Mvc;
using Domain;
using PublicApi.v1.DTO;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]   
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CommoditiesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        public CommoditiesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/Commodities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ApiCommodityDTO>>> GetCommodities()
        {
            return (await _bll.Commodities.AllAsync())
                .Select(e => PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(e)).ToList();
        }

        // GET: api/Commodities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ApiCommodityDTO>> GetCommodity(int id)
        {
            var commodity = PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(await _bll.Commodities.FindAsync(id));

            if (commodity == null)
            {
                return NotFound();
            }

            return commodity;
        }

        // PUT: api/Commodities/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCommodity(int id, ApiCommodityDTO commodity)
        {
            if (id != commodity.Id)
            {
                return BadRequest();
            }

            _bll.Commodities.Update(PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromExternal(commodity));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Commodities
        [HttpPost]
        public async Task<ActionResult<ApiCommodityDTO>> PostCommodity(ApiCommodityDTO commodity)
        {
            commodity = PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(
                _bll.Commodities.Add(PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromExternal(commodity)));
            await _bll.SaveChangesAsync();
            commodity = PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(
                _bll.Commodities.GetUpdatesAfterUOWSaveChanges(PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromExternal(commodity)));            

            return CreatedAtAction("GetCommodity", new {version = HttpContext.GetRequestedApiVersion().ToString(), id = commodity.Id }, commodity);
        }

        // DELETE: api/Commodities/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ApiCommodityDTO>> DeleteCommodity(int id)
        {
            var commodity = PublicApi.v1.Mappers.ApiCommoditiesMapper.MapFromBLL(await _bll.Commodities.FindAsync(id));
            if (commodity == null)
            {
                return NotFound();
            }

            _bll.Commodities.Remove(commodity.Id);
            await _bll.SaveChangesAsync();

            return commodity;
        }
    }
}
