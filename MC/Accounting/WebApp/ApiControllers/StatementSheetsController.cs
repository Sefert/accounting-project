using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]   
    public class StatementSheetsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        public StatementSheetsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/StatementSheets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiStatementSheetDTO>>> GetStatementSheets()
        {
            return (await _bll.Statements.GetAllStatementsAsync())
                .Select(e => PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(e)).ToList();
        }

        // GET: api/StatementReports/HoldingCompanies/5
        /*[HttpGet("HoldingCompanies/{id}")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiStatementSheetDTO>>> GetStatementReportByHoldingCompanyId(int id)
        {
            var statementSheets = (await _bll.Statements.GetStatementReportByHoldingCompanyId(id))
                .Select(e => PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(e)).ToList();

            return statementSheets;
        }*/
        
        // GET: api/StatementSheets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiStatementSheetDTO>> GetStatementSheet(int id)
        {
            var statementSheet = PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(await _bll.Statements.FindAsync(id));

            if (statementSheet == null)
            {
                return NotFound();
            }

            return statementSheet;
        }

        // PUT: api/StatementSheets/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatementSheet(int id, PublicApi.v1.DTO.ApiStatementSheetDTO statementSheet)
        {
            if (id != statementSheet.Id)
            {
                return BadRequest();
            }

            _bll.Statements.Update(PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromExternal(statementSheet));
            await _bll.SaveChangesAsync();
            
            return NoContent();
        }

        // POST: api/StatementSheets
        [HttpPost]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiStatementSheetDTO>> PostStatementSheet(PublicApi.v1.DTO.ApiStatementSheetDTO statementSheet)
        {
            statementSheet = PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(
                _bll.Statements.Add(PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromExternal(statementSheet)));
            //_bll.HoldingCompanies.Add(PublicApi.v1.Mappers.ApiHoldingCompanyMapper.MapFromExternal(holdingCompany));
            await _bll.SaveChangesAsync();
            statementSheet = PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(
                _bll.Statements.GetUpdatesAfterUOWSaveChanges(PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromExternal(statementSheet)));
            return CreatedAtAction("GetStatementSheet", new
            {
                version = HttpContext.GetRequestedApiVersion().ToString(),
                id = statementSheet.Id
            }, statementSheet);
        }

        // DELETE: api/StatementSheets/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiStatementSheetDTO>> DeleteStatementSheet(int id)
        {
            var statementSheet = PublicApi.v1.Mappers.ApiStatementSheetMapper.MapFromBLL(await _bll.Statements.FindAsync(id));
            if (statementSheet == null)
            {
                return NotFound();
            }

            _bll.Statements.Remove(statementSheet.Id);
            await _bll.SaveChangesAsync();

            return statementSheet;
        }
    }
}
