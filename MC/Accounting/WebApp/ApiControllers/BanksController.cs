using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]   
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BanksController : ControllerBase
    {
        private readonly IAppBLL _bll;

        public BanksController(IAppBLL bll)
        {
            _bll = bll;
        }
      
        /*[HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.ApiBankDTO>>> GetHoldingCompanyBanks(int id)
        {
            return (await _bll.Banks.GetCompanyBanksAsync(id))
                .Select(e => PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(e)).ToList(); 
        }*/
        
        // GET: api/Banks
        [HttpGet]
        public async Task<ActionResult<List<PublicApi.v1.DTO.ApiBankDTO>>> GetBanks()
        {
            return (await _bll.Banks.GetAllBanksAsync())
                .Select(e => PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(e)).ToList();
        }

        // GET: api/Banks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiBankDTO>> GetBank(int id)
        {
            var bank = PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(await _bll.Banks.FindAsync(id));

            if (bank == null)
            {
                return NotFound();
            }

            return bank;
        }

        // PUT: api/Banks/5
        
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBank(int id, PublicApi.v1.DTO.ApiBankDTO bank)
        {
            if (id != bank.Id)
            {
                return BadRequest();
            }

            _bll.Banks.Update(PublicApi.v1.Mappers.ApiBankMapper.MapFromExternal(bank));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Banks
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiBankDTO>> PostBank(PublicApi.v1.DTO.ApiBankDTO bank)
        {
            bank = PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(
                _bll.Banks.Add(PublicApi.v1.Mappers.ApiBankMapper.MapFromExternal(bank)));
            await _bll.SaveChangesAsync();
            bank = PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(
                _bll.Banks.GetUpdatesAfterUOWSaveChanges(PublicApi.v1.Mappers.ApiBankMapper.MapFromExternal(bank)));

            return CreatedAtAction("GetBank", new {version = HttpContext.GetRequestedApiVersion().ToString(), id = bank.Id }, bank);
        }

        // DELETE: api/Banks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.ApiBankDTO>> DeleteBank(int id)
        {
            var bank = PublicApi.v1.Mappers.ApiBankMapper.MapFromBLL(await _bll.Banks.FindAsync(id));
            if (bank == null)
            {
                return NotFound();
            }

            _bll.Banks.Remove(bank);
            await _bll.SaveChangesAsync();

            return bank;
        }
    }
}
