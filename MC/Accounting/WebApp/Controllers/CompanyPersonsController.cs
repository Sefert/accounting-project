using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class CompanyPersonsController : Controller
    {
        private readonly AppDbContext _context;

        public CompanyPersonsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: CompanyPersons
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.CompanyPersons.Include(c => c.Department).Include(c => c.HoldingCompany).Include(c => c.Person);
            return View(await appDbContext.ToListAsync());
        }

        // GET: CompanyPersons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyPerson = await _context.CompanyPersons
                .Include(c => c.Department)
                .Include(c => c.HoldingCompany)
                .Include(c => c.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyPerson == null)
            {
                return NotFound();
            }

            return View(companyPerson);
        }

        // GET: CompanyPersons/Create
        public IActionResult Create()
        {
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address");
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName");
            return View();
        }

        // POST: CompanyPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StartDate,EndDate,PersonId,HoldingCompanyId,DepartmentId,Id")] CompanyPerson companyPerson)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companyPerson);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", companyPerson.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", companyPerson.HoldingCompanyId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", companyPerson.PersonId);
            return View(companyPerson);
        }

        // GET: CompanyPersons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyPerson = await _context.CompanyPersons.FindAsync(id);
            if (companyPerson == null)
            {
                return NotFound();
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", companyPerson.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", companyPerson.HoldingCompanyId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", companyPerson.PersonId);
            return View(companyPerson);
        }

        // POST: CompanyPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StartDate,EndDate,PersonId,HoldingCompanyId,DepartmentId,Id")] CompanyPerson companyPerson)
        {
            if (id != companyPerson.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companyPerson);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyPersonExists(companyPerson.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", companyPerson.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", companyPerson.HoldingCompanyId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", companyPerson.PersonId);
            return View(companyPerson);
        }

        // GET: CompanyPersons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyPerson = await _context.CompanyPersons
                .Include(c => c.Department)
                .Include(c => c.HoldingCompany)
                .Include(c => c.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyPerson == null)
            {
                return NotFound();
            }

            return View(companyPerson);
        }

        // POST: CompanyPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyPerson = await _context.CompanyPersons.FindAsync(id);
            _context.CompanyPersons.Remove(companyPerson);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyPersonExists(int id)
        {
            return _context.CompanyPersons.Any(e => e.Id == id);
        }
    }
}
