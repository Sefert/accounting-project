using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class FiscalYearsController : Controller
    {
        private readonly AppDbContext _context;

        public FiscalYearsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: FiscalYears
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.FiscalYears.Include(f => f.Department).Include(f => f.HoldingCompany);
            return View(await appDbContext.ToListAsync());
        }

        // GET: FiscalYears/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fiscalYear = await _context.FiscalYears
                .Include(f => f.Department)
                .Include(f => f.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fiscalYear == null)
            {
                return NotFound();
            }

            return View(fiscalYear);
        }

        // GET: FiscalYears/Create
        public IActionResult Create()
        {
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address");
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            return View();
        }

        // POST: FiscalYears/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProjectName,Start,End,HoldingCompanyId,DepartmentId,Id")] FiscalYear fiscalYear)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fiscalYear);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", fiscalYear.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", fiscalYear.HoldingCompanyId);
            return View(fiscalYear);
        }

        // GET: FiscalYears/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fiscalYear = await _context.FiscalYears.FindAsync(id);
            if (fiscalYear == null)
            {
                return NotFound();
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", fiscalYear.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", fiscalYear.HoldingCompanyId);
            return View(fiscalYear);
        }

        // POST: FiscalYears/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProjectName,Start,End,HoldingCompanyId,DepartmentId,Id")] FiscalYear fiscalYear)
        {
            if (id != fiscalYear.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fiscalYear);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FiscalYearExists(fiscalYear.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", fiscalYear.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", fiscalYear.HoldingCompanyId);
            return View(fiscalYear);
        }

        // GET: FiscalYears/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fiscalYear = await _context.FiscalYears
                .Include(f => f.Department)
                .Include(f => f.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fiscalYear == null)
            {
                return NotFound();
            }

            return View(fiscalYear);
        }

        // POST: FiscalYears/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fiscalYear = await _context.FiscalYears.FindAsync(id);
            _context.FiscalYears.Remove(fiscalYear);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FiscalYearExists(int id)
        {
            return _context.FiscalYears.Any(e => e.Id == id);
        }
    }
}
