using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class CommoditiesController : Controller
    {
        private readonly AppDbContext _context;

        public CommoditiesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Commodities
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Commodities.Include(c => c.Commodities).Include(c => c.Department).Include(c => c.HoldingCompany);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Commodities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commodity = await _context.Commodities
                .Include(c => c.Commodities)
                .Include(c => c.Department)
                .Include(c => c.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (commodity == null)
            {
                return NotFound();
            }

            return View(commodity);
        }

        // GET: Commodities/Create
        public IActionResult Create()
        {
            ViewData["CommoditiesId"] = new SelectList(_context.Commodities, "Id", "ItemName");
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address");
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            return View();
        }

        // POST: Commodities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HoldingCompanyId,DepartmentId,CommoditiesId,ItemName,NetValue,UnitType,Id")] Commodity commodity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(commodity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommoditiesId"] = new SelectList(_context.Commodities, "Id", "ItemName", commodity.CommoditiesId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", commodity.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", commodity.HoldingCompanyId);
            return View(commodity);
        }

        // GET: Commodities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commodity = await _context.Commodities.FindAsync(id);
            if (commodity == null)
            {
                return NotFound();
            }
            ViewData["CommoditiesId"] = new SelectList(_context.Commodities, "Id", "ItemName", commodity.CommoditiesId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", commodity.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", commodity.HoldingCompanyId);
            return View(commodity);
        }

        // POST: Commodities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("HoldingCompanyId,DepartmentId,CommoditiesId,ItemName,NetValue,UnitType,Id")] Commodity commodity)
        {
            if (id != commodity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(commodity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommodityExists(commodity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommoditiesId"] = new SelectList(_context.Commodities, "Id", "ItemName", commodity.CommoditiesId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", commodity.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", commodity.HoldingCompanyId);
            return View(commodity);
        }

        // GET: Commodities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commodity = await _context.Commodities
                .Include(c => c.Commodities)
                .Include(c => c.Department)
                .Include(c => c.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (commodity == null)
            {
                return NotFound();
            }

            return View(commodity);
        }

        // POST: Commodities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var commodity = await _context.Commodities.FindAsync(id);
            _context.Commodities.Remove(commodity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CommodityExists(int id)
        {
            return _context.Commodities.Any(e => e.Id == id);
        }
    }
}
