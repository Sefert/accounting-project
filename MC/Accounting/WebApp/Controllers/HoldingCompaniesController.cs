using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class HoldingCompaniesController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly AppDbContext _context;

        public HoldingCompaniesController(IAppBLL bll, AppDbContext context)
        {
            _bll = bll;
            _context = context;
        }

        // GET: HoldingCompanies
        public async Task<IActionResult> Index()
        {
            var holdingCompanies = (await _bll.HoldingCompanies.GetAllHoldingCompaniesAsync())
                .Select(e => BLL.App.Mappers.BLLHoldingCompanyMapper.MapFromBLL(e))
                .Select(e => DAL.App.EF.Mappers.HoldingCompanyMapper.MapFromDAL(e));
            return View(holdingCompanies);
        }

        // GET: HoldingCompanies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var holdingCompany = await _bll.HoldingCompanies.FindAsync(id);
                //.Include(h => h.Subsidiary)
                //.FirstOrDefaultAsync(m => m.Id == id);
            if (holdingCompany == null)
            {
                return NotFound();
            }

            return View(holdingCompany);
        }

        // GET: HoldingCompanies/Create
        public IActionResult Create()
        {
            //ViewData["SubsidiaryId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            return View();
        }

        // POST: HoldingCompanies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RegistrationNumber,VatRegistration,SubsidiaryId,Name,Address,Id")] BLLHoldingCompanyDTO holdingCompany)
        {
            /*if (ModelState.IsValid)
            {
                await  _bll.HoldingCompanies.AddAsync(holdingCompany);
                await  _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            //ViewData["SubsidiaryId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", holdingCompany.SubsidiaryId);
            return View(BLL.App.Mappers.BLLHoldingCompanyMapper.MapFromDAL(holdingCompany));*/
            throw new NotImplementedException();
        }

        // GET: HoldingCompanies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            /*if (id == null)
            {
                return NotFound();
            }

            var holdingCompany = await  _bll.HoldingCompanies.FindAsync(id);
            if (holdingCompany == null)
            {
                return NotFound();
            }
            //ViewData["SubsidiaryId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", holdingCompany.SubsidiaryId);
            return View(holdingCompany);*/
            throw new NotImplementedException();
        }

        // POST: HoldingCompanies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RegistrationNumber,VatRegistration,SubsidiaryId,Name,Address,Id")] BLLHoldingCompanyDTO holdingCompany)
        {
            /*if (id != holdingCompany.Id)
            {
                return NotFound();
            }
            _bll.HoldingCompanies.Update(holdingCompany);
            await  _bll.SaveChangesAsync();
            
            //ViewData["SubsidiaryId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", holdingCompany.SubsidiaryId);
            return View(holdingCompany);*/
            throw new NotImplementedException();
        }

        // GET: HoldingCompanies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            /*if (id == null)
            {
                return NotFound();
            }

            var holdingCompany = await  _bll.HoldingCompanies.FindAsync(id);
                //.Include(h => h.Subsidiary)
                //.FirstOrDefaultAsync(m => m.Id == id);
            if (holdingCompany == null)
            {
                return NotFound();
            }

            return View(holdingCompany);*/
            throw new NotImplementedException();
        }

        // POST: HoldingCompanies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hc = await _context.HoldingCompanies.FirstOrDefaultAsync(su => su.Subsidiary.Id == id);
            
            _context.HoldingCompanies.Remove(hc);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
