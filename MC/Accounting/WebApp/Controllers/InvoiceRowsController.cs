using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class InvoiceRowsController : Controller
    {
        private readonly AppDbContext _context;

        public InvoiceRowsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: InvoiceRows
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.InvoiceRows.Include(i => i.Commodity).Include(i => i.Credit).Include(i => i.Debit).Include(i => i.Invoice).Include(i => i.Project);
            return View(await appDbContext.ToListAsync());
        }

        // GET: InvoiceRows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceRow = await _context.InvoiceRows
                .Include(i => i.Commodity)
                .Include(i => i.Credit)
                .Include(i => i.Debit)
                .Include(i => i.Invoice)
                .Include(i => i.Project)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (invoiceRow == null)
            {
                return NotFound();
            }

            return View(invoiceRow);
        }

        // GET: InvoiceRows/Create
        public IActionResult Create()
        {
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName");
            ViewData["CreditId"] = new SelectList(_context.StatementSheets, "Id", "Tally");
            ViewData["DebitId"] = new SelectList(_context.StatementSheets, "Id", "Tally");
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName");
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName");
            return View();
        }

        // POST: InvoiceRows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DiscountPercent,DebitId,CreditId,ProjectId,InvoiceId,CommodityId,Amount,ItemName,NetValue,UnitType,Id")] InvoiceRow invoiceRow)
        {
            if (ModelState.IsValid)
            {
                _context.Add(invoiceRow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", invoiceRow.CommodityId);
            ViewData["CreditId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.CreditId);
            ViewData["DebitId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.DebitId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", invoiceRow.InvoiceId);
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", invoiceRow.ProjectId);
            return View(invoiceRow);
        }

        // GET: InvoiceRows/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceRow = await _context.InvoiceRows.FindAsync(id);
            if (invoiceRow == null)
            {
                return NotFound();
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", invoiceRow.CommodityId);
            ViewData["CreditId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.CreditId);
            ViewData["DebitId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.DebitId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", invoiceRow.InvoiceId);
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", invoiceRow.ProjectId);
            return View(invoiceRow);
        }

        // POST: InvoiceRows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DiscountPercent,DebitId,CreditId,ProjectId,InvoiceId,CommodityId,Amount,ItemName,NetValue,UnitType,Id")] InvoiceRow invoiceRow)
        {
            if (id != invoiceRow.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(invoiceRow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoiceRowExists(invoiceRow.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", invoiceRow.CommodityId);
            ViewData["CreditId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.CreditId);
            ViewData["DebitId"] = new SelectList(_context.StatementSheets, "Id", "Tally", invoiceRow.DebitId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", invoiceRow.InvoiceId);
            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "ProjectName", invoiceRow.ProjectId);
            return View(invoiceRow);
        }

        // GET: InvoiceRows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceRow = await _context.InvoiceRows
                .Include(i => i.Commodity)
                .Include(i => i.Credit)
                .Include(i => i.Debit)
                .Include(i => i.Invoice)
                .Include(i => i.Project)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (invoiceRow == null)
            {
                return NotFound();
            }

            return View(invoiceRow);
        }

        // POST: InvoiceRows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var invoiceRow = await _context.InvoiceRows.FindAsync(id);
            _context.InvoiceRows.Remove(invoiceRow);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InvoiceRowExists(int id)
        {
            return _context.InvoiceRows.Any(e => e.Id == id);
        }
    }
}
