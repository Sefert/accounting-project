using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class StatementSheetsController : Controller
    {
        private readonly AppDbContext _context;

        public StatementSheetsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: StatementSheets
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.StatementSheets.Include(s => s.HoldingCompany);
            return View(await appDbContext.ToListAsync());
        }

        // GET: StatementSheets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementSheet = await _context.StatementSheets
                .Include(s => s.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statementSheet == null)
            {
                return NotFound();
            }

            return View(statementSheet);
        }

        // GET: StatementSheets/Create
        public IActionResult Create()
        {
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            return View();
        }

        // POST: StatementSheets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Tally,TransactionCode,HoldingCompanyId,Id")] StatementSheet statementSheet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statementSheet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", statementSheet.HoldingCompanyId);
            return View(statementSheet);
        }

        // GET: StatementSheets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementSheet = await _context.StatementSheets.FindAsync(id);
            if (statementSheet == null)
            {
                return NotFound();
            }
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", statementSheet.HoldingCompanyId);
            return View(statementSheet);
        }

        // POST: StatementSheets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Tally,TransactionCode,HoldingCompanyId,Id")] StatementSheet statementSheet)
        {
            if (id != statementSheet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statementSheet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatementSheetExists(statementSheet.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", statementSheet.HoldingCompanyId);
            return View(statementSheet);
        }

        // GET: StatementSheets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementSheet = await _context.StatementSheets
                .Include(s => s.HoldingCompany)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statementSheet == null)
            {
                return NotFound();
            }

            return View(statementSheet);
        }

        // POST: StatementSheets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var statementSheet = await _context.StatementSheets.FindAsync(id);
            _context.StatementSheets.Remove(statementSheet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StatementSheetExists(int id)
        {
            return _context.StatementSheets.Any(e => e.Id == id);
        }
    }
}
