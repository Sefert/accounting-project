using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly AppDbContext _context;

        public TransactionsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Transactions
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Transactions.Include(t => t.Invoice).Include(t => t.InvoiceRow).Include(t => t.StatementReport).Include(t => t.Warehouse);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Transactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions
                .Include(t => t.Invoice)
                .Include(t => t.InvoiceRow)
                .Include(t => t.StatementReport)
                .Include(t => t.Warehouse)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // GET: Transactions/Create
        public IActionResult Create()
        {
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName");
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName");
            ViewData["StatementReportId"] = new SelectList(_context.StatementReports, "Id", "Id");
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TransactionDate,StatementReportId,WarehouseId,InvoiceRowId,InvoiceId,Id")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                _context.Add(transaction);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", transaction.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", transaction.InvoiceRowId);
            ViewData["StatementReportId"] = new SelectList(_context.StatementReports, "Id", "Id", transaction.StatementReportId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", transaction.WarehouseId);
            return View(transaction);
        }

        // GET: Transactions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", transaction.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", transaction.InvoiceRowId);
            ViewData["StatementReportId"] = new SelectList(_context.StatementReports, "Id", "Id", transaction.StatementReportId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", transaction.WarehouseId);
            return View(transaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TransactionDate,StatementReportId,WarehouseId,InvoiceRowId,InvoiceId,Id")] Transaction transaction)
        {
            if (id != transaction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransactionExists(transaction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", transaction.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", transaction.InvoiceRowId);
            ViewData["StatementReportId"] = new SelectList(_context.StatementReports, "Id", "Id", transaction.StatementReportId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", transaction.WarehouseId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transactions
                .Include(t => t.Invoice)
                .Include(t => t.InvoiceRow)
                .Include(t => t.StatementReport)
                .Include(t => t.Warehouse)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transaction = await _context.Transactions.FindAsync(id);
            _context.Transactions.Remove(transaction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransactionExists(int id)
        {
            return _context.Transactions.Any(e => e.Id == id);
        }
    }
}
