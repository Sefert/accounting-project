using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class SettingsController : Controller
    {
        private readonly AppDbContext _context;

        public SettingsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Settings
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Settings.Include(s => s.Commodity).Include(s => s.Department).Include(s => s.HoldingCompany).Include(s => s.Invoice).Include(s => s.StatementSheet).Include(s => s.Warehouse);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Settings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var setting = await _context.Settings
                .Include(s => s.Commodity)
                .Include(s => s.Department)
                .Include(s => s.HoldingCompany)
                .Include(s => s.Invoice)
                .Include(s => s.StatementSheet)
                .Include(s => s.Warehouse)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (setting == null)
            {
                return NotFound();
            }

            return View(setting);
        }

        // GET: Settings/Create
        public IActionResult Create()
        {
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName");
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address");
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address");
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName");
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally");
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName");
            return View();
        }

        // POST: Settings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FileName,ContentType,FilePath,HoldingCompanyId,DepartmentId,InvoiceId,CommodityId,WarehouseId,StatementSheetId,Id")] Setting setting)
        {
            if (ModelState.IsValid)
            {
                _context.Add(setting);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", setting.CommodityId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", setting.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", setting.HoldingCompanyId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", setting.InvoiceId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", setting.StatementSheetId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", setting.WarehouseId);
            return View(setting);
        }

        // GET: Settings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var setting = await _context.Settings.FindAsync(id);
            if (setting == null)
            {
                return NotFound();
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", setting.CommodityId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", setting.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", setting.HoldingCompanyId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", setting.InvoiceId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", setting.StatementSheetId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", setting.WarehouseId);
            return View(setting);
        }

        // POST: Settings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FileName,ContentType,FilePath,HoldingCompanyId,DepartmentId,InvoiceId,CommodityId,WarehouseId,StatementSheetId,Id")] Setting setting)
        {
            if (id != setting.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(setting);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SettingExists(setting.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CommodityId"] = new SelectList(_context.Commodities, "Id", "ItemName", setting.CommodityId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Address", setting.DepartmentId);
            ViewData["HoldingCompanyId"] = new SelectList(_context.HoldingCompanies, "Id", "Address", setting.HoldingCompanyId);
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", setting.InvoiceId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", setting.StatementSheetId);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "Id", "ItemName", setting.WarehouseId);
            return View(setting);
        }

        // GET: Settings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var setting = await _context.Settings
                .Include(s => s.Commodity)
                .Include(s => s.Department)
                .Include(s => s.HoldingCompany)
                .Include(s => s.Invoice)
                .Include(s => s.StatementSheet)
                .Include(s => s.Warehouse)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (setting == null)
            {
                return NotFound();
            }

            return View(setting);
        }

        // POST: Settings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var setting = await _context.Settings.FindAsync(id);
            _context.Settings.Remove(setting);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SettingExists(int id)
        {
            return _context.Settings.Any(e => e.Id == id);
        }
    }
}
