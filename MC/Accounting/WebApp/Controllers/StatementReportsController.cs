using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class StatementReportsController : Controller
    {
        private readonly AppDbContext _context;

        public StatementReportsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: StatementReports
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.StatementReports.Include(s => s.FiscalYear).Include(s => s.StatementSheet);
            return View(await appDbContext.ToListAsync());
        }

        // GET: StatementReports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementReport = await _context.StatementReports
                .Include(s => s.FiscalYear)
                .Include(s => s.StatementSheet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statementReport == null)
            {
                return NotFound();
            }

            return View(statementReport);
        }

        // GET: StatementReports/Create
        public IActionResult Create()
        {
            ViewData["FiscalYearId"] = new SelectList(_context.FiscalYears, "Id", "ProjectName");
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally");
            return View();
        }

        // POST: StatementReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Total,FiscalYearId,StatementSheetId,Id")] StatementReport statementReport)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statementReport);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FiscalYearId"] = new SelectList(_context.FiscalYears, "Id", "ProjectName", statementReport.FiscalYearId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", statementReport.StatementSheetId);
            return View(statementReport);
        }

        // GET: StatementReports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementReport = await _context.StatementReports.FindAsync(id);
            if (statementReport == null)
            {
                return NotFound();
            }
            ViewData["FiscalYearId"] = new SelectList(_context.FiscalYears, "Id", "ProjectName", statementReport.FiscalYearId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", statementReport.StatementSheetId);
            return View(statementReport);
        }

        // POST: StatementReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Total,FiscalYearId,StatementSheetId,Id")] StatementReport statementReport)
        {
            if (id != statementReport.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statementReport);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatementReportExists(statementReport.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FiscalYearId"] = new SelectList(_context.FiscalYears, "Id", "ProjectName", statementReport.FiscalYearId);
            ViewData["StatementSheetId"] = new SelectList(_context.StatementSheets, "Id", "Tally", statementReport.StatementSheetId);
            return View(statementReport);
        }

        // GET: StatementReports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statementReport = await _context.StatementReports
                .Include(s => s.FiscalYear)
                .Include(s => s.StatementSheet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statementReport == null)
            {
                return NotFound();
            }

            return View(statementReport);
        }

        // POST: StatementReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var statementReport = await _context.StatementReports.FindAsync(id);
            _context.StatementReports.Remove(statementReport);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StatementReportExists(int id)
        {
            return _context.StatementReports.Any(e => e.Id == id);
        }
    }
}
