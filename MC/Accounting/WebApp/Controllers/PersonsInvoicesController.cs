using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using DAL.App.EF;
using Domain;

namespace WebApp.Controllers
{
    public class PersonsInvoicesController : Controller
    {
        private readonly AppDbContext _context;

        public PersonsInvoicesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: PersonsInvoices
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.PersonsInvoices.Include(p => p.Invoice).Include(p => p.InvoiceRow).Include(p => p.Person);
            return View(await appDbContext.ToListAsync());
        }

        // GET: PersonsInvoices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personsInvoice = await _context.PersonsInvoices
                .Include(p => p.Invoice)
                .Include(p => p.InvoiceRow)
                .Include(p => p.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personsInvoice == null)
            {
                return NotFound();
            }

            return View(personsInvoice);
        }

        // GET: PersonsInvoices/Create
        public IActionResult Create()
        {
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName");
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName");
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName");
            return View();
        }

        // POST: PersonsInvoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InvoiceId,PersonId,InvoiceRowId,Id")] PersonsInvoice personsInvoice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(personsInvoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", personsInvoice.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", personsInvoice.InvoiceRowId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personsInvoice.PersonId);
            return View(personsInvoice);
        }

        // GET: PersonsInvoices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personsInvoice = await _context.PersonsInvoices.FindAsync(id);
            if (personsInvoice == null)
            {
                return NotFound();
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", personsInvoice.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", personsInvoice.InvoiceRowId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personsInvoice.PersonId);
            return View(personsInvoice);
        }

        // POST: PersonsInvoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InvoiceId,PersonId,InvoiceRowId,Id")] PersonsInvoice personsInvoice)
        {
            if (id != personsInvoice.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personsInvoice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonsInvoiceExists(personsInvoice.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["InvoiceId"] = new SelectList(_context.Invoices, "Id", "FileName", personsInvoice.InvoiceId);
            ViewData["InvoiceRowId"] = new SelectList(_context.InvoiceRows, "Id", "ItemName", personsInvoice.InvoiceRowId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personsInvoice.PersonId);
            return View(personsInvoice);
        }

        // GET: PersonsInvoices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personsInvoice = await _context.PersonsInvoices
                .Include(p => p.Invoice)
                .Include(p => p.InvoiceRow)
                .Include(p => p.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personsInvoice == null)
            {
                return NotFound();
            }

            return View(personsInvoice);
        }

        // POST: PersonsInvoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var personsInvoice = await _context.PersonsInvoices.FindAsync(id);
            _context.PersonsInvoices.Remove(personsInvoice);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonsInvoiceExists(int id)
        {
            return _context.PersonsInvoices.Any(e => e.Id == id);
        }
    }
}
