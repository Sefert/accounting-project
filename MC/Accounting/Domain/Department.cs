using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Department : BaseCompany
    {       
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }        
    }
}