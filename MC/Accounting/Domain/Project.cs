using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Project : BaseEntity
    {
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Must be between 3 and 100 character in length.")]
        public string ProjectName { get; set; }
        
        [Required]
        public DateTime Start { get; set; }
        
        public DateTime? End { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<InvoiceRow> InvoiceRows { get; set; }
    }
}