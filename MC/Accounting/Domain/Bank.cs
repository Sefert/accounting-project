using System.ComponentModel.DataAnnotations;
using Contracts.DAL.Base;

namespace Domain
{
    public class Bank : BaseEntity, IDomainEntity
    {
        [Required]
        [StringLength(255, ErrorMessage = "Must be less then 255 characters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Must be less then 50 characters")]
        public string Account { get; set; }     
        
        public string Iban { get; set; } 
        
        public string SwiftBic { get; set; } 
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
    }
}