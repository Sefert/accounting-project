using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public abstract class BaseWarehouse : BaseCommodity
    {
        [Column(TypeName = "decimal(8,2)")]
        public decimal Amount { get; set; }
    }
}