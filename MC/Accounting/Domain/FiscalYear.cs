using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Contracts.DAL.Base;

namespace Domain
{
    public class FiscalYear : BaseEntity, IDomainEntity
    {
        [Required]
        [StringLength(80, MinimumLength = 3, ErrorMessage = "Must be between 3 and 80 character in length.")]
        public string ProjectName { get; set; }
        
        [Required]
        public DateTime Start { get; set; }
        
        public DateTime? End { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public ICollection<StatementReport> BalanceIncome { get; set; } 
    }
}