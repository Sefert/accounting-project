﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.DAL.Base;
using Newtonsoft.Json;

namespace Domain
{
    public class HoldingCompany : BaseCompany, IDomainEntity
    {       
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string RegistrationNumber { get; set; }
       
        [Required]
        [StringLength(40, ErrorMessage = "Must less then 40 letters")]
        public string VatRegistration { get; set; }       
        
        public ICollection<Bank> BankInfos { get; set; }
        public ICollection<Department> Departments { get; set; } 
        
        //[JsonIgnore]
        [InverseProperty("Subsidiary")]
        public ICollection<HoldingCompany> Subsidiaries { get; set; } 
        
        public int? SubsidiaryId { get; set; }
        public HoldingCompany Subsidiary { get; set; } 
    }
}