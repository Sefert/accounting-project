using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class StatementReport : BaseEntity
    {                  
        [Column(TypeName = "decimal(15,3)")]
        public decimal Total { get; set; }

        public int FiscalYearId { get; set; }
        public FiscalYear FiscalYear { get; set; }
        
        public int? StatementSheetId { get; set; }
        public StatementSheet StatementSheet { get; set; }
        
        public ICollection<Transaction> Transactions { get; set; }
    }
}