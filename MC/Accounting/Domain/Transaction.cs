using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Transaction : BaseEntity
    {       
        [Required]
        public DateTime TransactionDate { get; set; } 
        
        public int StatementReportId { get; set; } 
        public StatementReport StatementReport { get; set; }
         
        public int? WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; } 
        
        public int? InvoiceRowId { get; set; }
        public InvoiceRow InvoiceRow { get; set; } 
        
        public int? InvoiceId { get; set; }
        public Invoice Invoice{ get; set; }       
    }
}