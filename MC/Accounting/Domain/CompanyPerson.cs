using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class CompanyPerson : BaseEntity
    {
        [Required]
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        
        public int? PersonId { get; set; }
        public Person Person { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}