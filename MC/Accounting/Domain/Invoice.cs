using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Identity;

namespace Domain
{
    public class Invoice : BaseEntity
    {    
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string InvoiceNumber { get; set; }
        
        [Required]
        public DateTime InvoiceDate { get; set; }
        
        [Required]
        public DateTime DueDate { get; set; }
        
        [Column(TypeName = "decimal(11,3)")]
        public decimal TotalAmount { get; set; }
        
        [Column(TypeName = "decimal(5,2)")]
        public decimal TaxationPercent { get; set; }
        
        [Column(TypeName = "decimal(5,2)")]
        public decimal DiscountPercent { get; set; }
        
        [Required]
        public Boolean TransactionConfirmed { get; set; }
        
        [Required]
        public string InvoicePath { get; set; }
        
        [Required]
        public string FileName { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public int? ProjectId { get; set; }
        public Project Project { get; set; }
        
        public int? CurrencyId { get; set; }
        public Currency Currency { get; set; }
        
        public ICollection<Setting> Settings { get; set; }
        public ICollection<InvoiceRow> InvoiceRows { get; set; }
        public ICollection<PersonsInvoice> PersonsInvoices { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
        
    }
}