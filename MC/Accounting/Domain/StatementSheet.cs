using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.DAL.Base;

namespace Domain
{
    public class StatementSheet : BaseEntity, IDomainEntity
    {
        [Required]
        [MaxLength(50)]
        public string Tally { get; set; }
        
        [Required]
        [MaxLength(16)]
        public string TransactionCode { get; set; }
        
        public int HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        [ForeignKey("DebitId")]
        [InverseProperty("Debit")]
        public ICollection<InvoiceRow> Debits { get; set; }
        
        [ForeignKey("CreditId")]
        [InverseProperty("Credit")]
        public ICollection<InvoiceRow> Credits { get; set; }
        
        public ICollection<Setting> Settings { get; set; }
        public ICollection<StatementReport> StatementReports { get; set; }
    }
}