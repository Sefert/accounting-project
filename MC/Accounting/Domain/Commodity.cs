using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.DAL.Base;

namespace Domain
{
    public class Commodity : BaseCommodity, IDomainEntity
    {       
        public int? HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
         
        [InverseProperty("Commodities")]
        public ICollection<Commodity> CommodityList { get; set; } 
        
        public int? CommoditiesId { get; set; }
        public Commodity Commodities { get; set; }
        
        //[ForeignKey("CommodityId")]
        //public ICollection<Commodity> WarehouseCommodity { get; set; }
        public ICollection<Warehouse> ItemList{ get; set; }
        public ICollection<Setting> Settings{ get; set; }
    }
}