using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Warehouse : BaseWarehouse
    {      
        public int? HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; } 
        
        public int CommodityId { get; set; }
        public Commodity Commodity { get; set; } 
        
        public ICollection<Setting> Settings { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}