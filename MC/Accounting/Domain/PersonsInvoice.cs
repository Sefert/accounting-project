using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class PersonsInvoice : BaseEntity
    {
        public int InvoiceId { get; set; }
        public Invoice Invoice { get; set; }  
        
        public int PersonId { get; set; }
        public Person Person { get; set; }  
        
        public int? InvoiceRowId { get; set; }
        public InvoiceRow InvoiceRow { get; set; } 
    }
}