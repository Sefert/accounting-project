using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public abstract class BaseCompany : BaseEntity
    {
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "Must be less then 100 letters")]
        public string Address { get; set; }

        public ICollection<Setting> Settings { get; set; }
        public ICollection<Invoice> Invoices { get; set; } 
        public ICollection<Project> Projects { get; set; } 
        public ICollection<Warehouse> Warehouses { get; set; } 
        public ICollection<FiscalYear> FiscalYears { get; set; } 
        public ICollection<CompanyPerson> CompanyPersons { get; set; } 
        public ICollection<Commodity> Commodities { get; set; } 
        public ICollection<StatementSheet> StatementSheets { get; set; }
    }
}