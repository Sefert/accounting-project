using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Setting : BaseEntity
    {
        //Preliminary, may change during the course!!!

        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Must be between 3 and 255 character in length.")]
        public string FileName { get; set; }
        
        [Required]
        [StringLength(30)]
        public string ContentType { get; set; }
        
        [Required]
        public string FilePath { get; set; }
        
        public int? HoldingCompanyId { get; set; }
        public HoldingCompany HoldingCompany { get; set; }
        
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public int? InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public Commodity Commodity { get; set; }
        
        public int? WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
        
        public int? StatementSheetId { get; set; }
        public StatementSheet StatementSheet { get; set; }

    }
}