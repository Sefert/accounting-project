using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class InvoiceRow : BaseWarehouse
    {     
        [MaxLength(5)]
        public string DiscountPercent { get; set; }
        
        public int DebitId { get; set; }
        public StatementSheet Debit { get; set; }
        
        public int CreditId { get; set; }
        public StatementSheet Credit { get; set; }
        
        public int? ProjectId { get; set; }
        public Project Project { get; set; }
        
        public int? InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
        
        public int? CommodityId { get; set; }
        public Commodity Commodity { get; set; }
        
        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<PersonsInvoice> PersonsInvoices { get; set; }
    }
}