using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;


namespace PublicApi.v1.Mappers
{
    public class ApiCommoditiesMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.ApiCommodityDTO))
            {
                return MapFromBLL((internalDTO.BLLCommodityDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BLLCommodityDTO))
            {
                return MapFromExternal((externalDTO.ApiCommodityDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.ApiCommodityDTO MapFromBLL(internalDTO.BLLCommodityDTO commodity)
        {
            var res = commodity == null ? null : new externalDTO.ApiCommodityDTO()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromBLL(commodity.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.BLLCommodityDTO MapFromExternal(externalDTO.ApiCommodityDTO commodity)
        {
            var res = commodity == null ? null : new internalDTO.BLLCommodityDTO()
            {
                Id = commodity.Id,
                ItemName = commodity.ItemName,
                NetValue = commodity.NetValue,
                UnitType = commodity.UnitType,
                HoldingCompanyId = commodity.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromExternal(commodity.HoldingCompany)
            };

            return res;
        }
    }
}