using System;
using System.Linq;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
     public class ApiHoldingCompanyMapper
         {
             public TOutObject Map<TOutObject>(object inObject)
                 where TOutObject : class
             {
                 if (typeof(TOutObject) == typeof(externalDTO.ApiHoldingCompanyDTO))
                 {
                     return MapFromBLL((internalDTO.BLLHoldingCompanyDTO) inObject) as TOutObject;
                 }
     
                 if (typeof(TOutObject) == typeof(internalDTO.BLLHoldingCompanyDTO))
                 {
                     return MapFromExternal((externalDTO.ApiHoldingCompanyDTO) inObject) as TOutObject;
                 }
     
                 throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
             }
     
     
             public static externalDTO.ApiHoldingCompanyDTO MapFromBLL(internalDTO.BLLHoldingCompanyDTO holdingCompany)
             {
                 var res = holdingCompany == null ? null : new externalDTO.ApiHoldingCompanyDTO()
                 {
                     Id = holdingCompany.Id,
                     Name = holdingCompany.Name,
                     Address = holdingCompany.Address,
                     RegistrationNumber = holdingCompany.RegistrationNumber,
                     VatRegistration = holdingCompany.VatRegistration,
                     SubsidiaryId = holdingCompany.SubsidiaryId,
                     //Subsidiary = MapFromBLL(holdingCompany.Subsidiary)
                     //StatementSheets =  holdingCompany.StatementSheets?.Select(ss => ApiStatementSheetMapper.MapFromBLL(ss)).ToList()
                 };
     
                 return res;
             }
             
             public static internalDTO.BLLHoldingCompanyDTO MapFromExternal(externalDTO.ApiHoldingCompanyDTO holdingCompany)
             {
                 var res = holdingCompany == null ? null : new internalDTO.BLLHoldingCompanyDTO()
                 {
                     Id = holdingCompany.Id,
                     Name = holdingCompany.Name,
                     Address = holdingCompany.Address,
                     RegistrationNumber = holdingCompany.RegistrationNumber,
                     VatRegistration = holdingCompany.VatRegistration,
                     SubsidiaryId = holdingCompany.SubsidiaryId,
                     //Subsidiary = MapFromExternal(holdingCompany.Subsidiary)
                     //StatementSheets =  holdingCompany.StatementSheets?.Select(ss => ApiStatementSheetMapper.MapFromExternal(ss)).ToList()
                 };
     
                 return res;
             }
         }
}