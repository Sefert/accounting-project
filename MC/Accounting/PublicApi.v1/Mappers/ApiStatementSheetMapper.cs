using System;
using internalDTO = BLL.App.DTO;
using externalDTO = PublicApi.v1.DTO;

namespace PublicApi.v1.Mappers
{
    public class ApiStatementSheetMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.ApiStatementSheetDTO))
            {
                return MapFromBLL((internalDTO.BLLStatementSheetDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BLLStatementSheetDTO))
            {
                return MapFromExternal((externalDTO.ApiStatementSheetDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException(
                $"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.ApiStatementSheetDTO MapFromBLL(internalDTO.BLLStatementSheetDTO statementSheet)
        {
            var res = statementSheet == null
                ? null
                : new externalDTO.ApiStatementSheetDTO()
                {
                    Id = statementSheet.Id,
                    Tally = statementSheet.Tally,
                    TransactionCode = statementSheet.TransactionCode,
                    HoldingCompanyId = statementSheet.HoldingCompanyId,
                    HoldingCompany = ApiHoldingCompanyMapper.MapFromBLL(statementSheet.HoldingCompany),
                };

            return res;
        }

        public static internalDTO.BLLStatementSheetDTO MapFromExternal(externalDTO.ApiStatementSheetDTO statementSheet)
        {
            var res = statementSheet == null
                ? null
                : new internalDTO.BLLStatementSheetDTO()
                {
                    Id = statementSheet.Id,
                    Tally = statementSheet.Tally,
                    TransactionCode = statementSheet.TransactionCode,
                    HoldingCompanyId = statementSheet.HoldingCompanyId,
                    HoldingCompany = ApiHoldingCompanyMapper.MapFromExternal(statementSheet.HoldingCompany),
                };

            return res;
        }
    }
}