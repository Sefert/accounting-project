using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class ApiBankMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.ApiBankDTO))
            {
                return MapFromBLL((internalDTO.BLLBankDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BLLBankDTO))
            {
                return MapFromExternal((externalDTO.ApiBankDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.ApiBankDTO MapFromBLL(internalDTO.BLLBankDTO bank)
        {
            var res = bank == null ? null : new externalDTO.ApiBankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromBLL(bank.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.BLLBankDTO MapFromExternal(externalDTO.ApiBankDTO bank)
        {
            var res = bank == null ? null : new internalDTO.BLLBankDTO()
            {
                Id = bank.Id,
                Name = bank.Name,
                Account = bank.Account,
                Iban = bank.Iban,
                SwiftBic = bank.SwiftBic,
                HoldingCompanyId = bank.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromExternal(bank.HoldingCompany)
            };

            return res;
        }
    }
}