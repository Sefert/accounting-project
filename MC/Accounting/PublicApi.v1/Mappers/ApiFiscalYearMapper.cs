using System;
using internalDTO = BLL.App.DTO;
using externalDTO = PublicApi.v1.DTO;

namespace PublicApi.v1.Mappers
{
    public class ApiFiscalYearMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.ApiFiscalYearDTO))
            {
                return MapFromBLL((internalDTO.BLLFiscalYearDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BLLFiscalYearDTO))
            {
                return MapFromExternal((externalDTO.ApiFiscalYearDTO) inObject) as TOutObject;
            }

            throw new InvalidCastException($"No conversion from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");
        }


        public static externalDTO.ApiFiscalYearDTO MapFromBLL(internalDTO.BLLFiscalYearDTO fiscalYear)
        {
            var res = fiscalYear == null ? null : new externalDTO.ApiFiscalYearDTO()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromBLL(fiscalYear.HoldingCompany)
            };

            return res;
        }
        
        public static internalDTO.BLLFiscalYearDTO MapFromExternal(externalDTO.ApiFiscalYearDTO fiscalYear)
        {
            var res = fiscalYear == null ? null : new internalDTO.BLLFiscalYearDTO()
            {
                Id = fiscalYear.Id,
                ProjectName = fiscalYear.ProjectName,
                Start = fiscalYear.Start,
                End = fiscalYear.End,
                HoldingCompanyId = fiscalYear.HoldingCompanyId,
                HoldingCompany = ApiHoldingCompanyMapper.MapFromExternal(fiscalYear.HoldingCompany)
            };

            return res;
        }
    }
}