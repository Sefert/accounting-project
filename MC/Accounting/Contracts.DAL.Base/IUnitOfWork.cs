using System.Threading.Tasks;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Repositories;

namespace Contracts
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync();
        int SaveChanges();
        
        //IBaseRepositoryAsync<TEntity> BaseRepository<TEntity>() where TEntity :  class, IBaseEntity, new();
    }
}