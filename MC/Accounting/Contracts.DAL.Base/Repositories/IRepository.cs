using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contracts.Repositories
{
    public interface IRepository<TEntity> : IRepositoryAsync<TEntity>, IRepositorySynchronous<TEntity>
        where TEntity: class, new()                                              
    {                                        
    }      
    
    public interface IRepositorySynchronous<TEntity> : IRepositoryCommon<TEntity>
        where TEntity: class, new()
    {
        IEnumerable<TEntity> All();
        
        TEntity Find(params object[] id);

        void Add(TEntity entity);

        int SaveChanges();
    }
    
    public interface IRepositoryAsync<TEntity> : IRepositoryCommon<TEntity>
        where TEntity: class, new()                     
    {                                                                      
        Task<IEnumerable<TEntity>> AllAsync();          
                                                             
        Task<TEntity> FindAsync(params object[] id);    
                                                                         
        Task AddAsync(TEntity entity);

        Task<int> SaveChangesAsync();
    }                                                   
    
    public interface IRepositoryCommon<TEntity>               
        where TEntity: class, new()                     
    {                                          
        TEntity Update(TEntity entity);                 
                                                    
        void Remove(TEntity entity);                    
        void Remove(params object[] id);                
    }                                                   
}