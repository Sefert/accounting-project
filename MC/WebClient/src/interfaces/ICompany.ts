import {IBaseEntity} from "./IBaseEntity";

export interface ICompany extends IBaseEntity{
  "name": string,
  "address": string,
  "registrationNumber": string | null,
  "vatRegistration": string | null
  "subsidiaryId": number | null
}
