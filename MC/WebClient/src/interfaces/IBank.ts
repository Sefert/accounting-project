import {IBaseEntity} from "./IBaseEntity";

export interface IBank extends IBaseEntity{
  "name": string,
  "account": string,
  "iban": string | null,
  "swiftBic":string | null,
  "holdingCompanyId": number,
}
