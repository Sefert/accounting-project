import {IBaseEntity} from "./IBaseEntity";
import {ICompany} from "./ICompany";

export interface IStatementSheet extends IBaseEntity{
  "tally": string,
  "transactionCode": string,
  "holdingCompanyId": number | null,
  "HoldingCompany": ICompany | null
}
