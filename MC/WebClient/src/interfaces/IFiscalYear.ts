import {IBaseEntity} from "./IBaseEntity";

export interface IFiscalYear extends IBaseEntity{
  "projectName": string,
  "start": string,
  "end": string | null,
  "holdingCompanyId": number
}
