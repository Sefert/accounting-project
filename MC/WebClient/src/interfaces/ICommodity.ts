import {IBaseEntity} from "./IBaseEntity";

export interface ICommodity extends IBaseEntity{
  "itemName": string,
  "netValue": number,
  "unitType": string,
  "holdingCompanyId": number,
}
