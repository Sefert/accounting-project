import {LogManager, View, autoinject} from "aurelia-framework";
import {activationStrategy, Redirect, RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {BankService} from "../services/bank-service";
import {IBank} from "../interfaces/IBank";
import {json} from "aurelia-fetch-client";
import {IdentityService} from "../services/identity-service";
import {AppConfig} from "../app-config";
import {Login} from "./login";

export var log = LogManager.getLogger('app.identity.logout');

@autoinject
export class Logout {
  
  constructor(
    private identityService: IdentityService,
    private appConfig: AppConfig,
    private router: Router,
    private login: Login
  ){
    log.debug('constructor');
  }
  
  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }

  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.appConfig.jwt = null;
    this.router.navigateToRoute('home').then(
      this.router.viewPortDefaults);
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
