import {LogManager, autoinject, View} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {RouterConfiguration, Router} from 'aurelia-router';
import {IdentityService} from "./services/identity-service";
import {AppConfig} from "./app-config";

export var log = LogManager.getLogger('app.Home');

@autoinject
export class Home {

  constructor(private identityService: IdentityService,private appConfig: AppConfig, private router: Router)
  {
    log.debug('constructor');
  }

  manageStatements() {
    this.router.navigateToRoute('statementsIndex');
  }
  manageCompanies() {
    this.router.navigateToRoute('companiesIndex');
  }
  manageFiscalYears() {
    this.router.navigateToRoute('fiscalYearsIndex');
  }
  manageBanks() {
    this.router.navigateToRoute('banksIndex');
  }
  manageCommodities() {
    this.router.navigateToRoute('commoditiesIndex');
  }
  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }

  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
