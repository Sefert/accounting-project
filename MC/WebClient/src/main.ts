import '@babel/polyfill';
import {Aurelia} from 'aurelia-framework'
import environment from './environment';
import {PLATFORM} from 'aurelia-pal';
import 'materialize-css';

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    // Install and configure the plugin
    .plugin(PLATFORM.moduleName('aurelia-materialize-bridge'),  (b: any)  => b.useAll())
    .plugin(PLATFORM.moduleName('aurelia-dialog'), (configuration: any) => {
      // use only attach-focus
      configuration.useResource('attach-focus')});
  
  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('main-router')));
}
