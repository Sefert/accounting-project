import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {bindable, observable} from "aurelia-typed-observable-plugin";
import {CompanyService} from "../services/company-service";
import {ICompany} from "../interfaces/ICompany";
import {SharedService} from "../services/shared-service";
import {IFiscalYear} from "../interfaces/IFiscalYear";
import {FiscalYearService} from "../services/fiscal-year-service";


export var log = LogManager.getLogger('app.fiscalYears.index');

@autoinject
export class Index {

  private fiscalYears: IFiscalYear[] = [];
  private showedFiscalYears: IFiscalYear[] = [];
  private companies: ICompany[] = [];
  private hc: ICompany;
  dateOption = {day : 'numeric', month : 'numeric', year : 'numeric'};
  
  @observable private editFiscalYear : IFiscalYear | null;
  @observable private addFiscalYear : IFiscalYear | null;
  @observable fiscalYearQuery : string;
  @observable companyQuery: string;
  @observable private errorReport: string | null = null;
  @observable fiscalYearDate: string;

  constructor
  (
    private fiscalYearService: FiscalYearService,
    private companyService: CompanyService,
    private dialogService: DialogService,
    private controller: DialogController,
    private sharedService: SharedService<ICompany>
  )
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openFiscalYearDeleteModal(fiscalYear: IFiscalYear) {
    this.dialogService.open( {viewModel: DeleteModal, model: fiscalYear}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.fiscalYearService.delete(fiscalYear.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeFiscalYear(fiscalYear);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    });
  }

  companyQueryChanged(){
    this.hc = this.getCompany()!;
    if (this.hc != undefined) {
      this.fiscalYearService.fetchAllByUrl('HoldingCompanies' + '/' + this.hc.id + '/' + 'FiscalYears').then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.fiscalYears = jsonData;
          this.showedFiscalYears = this.fiscalYears;
          this.errorReport = null;
        });
    }
  }
  
  /*fiscalYearDateChanged(newDate: string, oldDate: string){
    this.fiscalYears[0].start = new Date(newDate).toJSON()
  }*/
  
  private getCompany(){
    return this.companies.find(c => c.name +" "+ c.vatRegistration == this.companyQuery);
  }
  
  createFiscalYear(){
    try {
      this.errorReport = null;
      console.log(this.companyQuery);
      if(this.companyQuery != null){
        this.hc = this.getCompany()!;
        log.debug('company', this.hc);
        this.fiscalYears.unshift({
          "id" : 0,
          "projectName" : "New Name",
          "start" : new Date().toJSON(),
          "end" : null,
          "holdingCompanyId" : this.hc.id
        });
        this.addFiscalYear = this.fiscalYears[0];
        this.editFiscalYear = this.fiscalYears[0];
        this.fiscalYearDate = new Date(this.fiscalYears[0].start)
          .toLocaleDateString('en-GB', {day : 'numeric', month : 'numeric', year : 'numeric'});
      } else {
        console.log("No company selected");
        this.errorReport = "Please select company!";
      }
    } catch (e) {
      this.errorReport = "Please select company!";
      console.log("Error");
    }
  }

  postFiscalYear(fiscalYear: IFiscalYear){
    fiscalYear.start = new Date(fiscalYear.start).toJSON();
    console.log(fiscalYear);
    this.fiscalYearService.post(fiscalYear).then(
      response =>{
        if (response.status == 201) {
          this.addFiscalYear = null;
          this.editFiscalYear = null;
          this.fetchFiscalYears();
        }else {
          log.error('Error in response', response);
        }
      }
    );
  }

  cancelPostFiscalYear(fiscalYear: IFiscalYear):void{
    this.addFiscalYear = null;
    this.editFiscalYear = null;
    this.removeFiscalYear(fiscalYear);
  }
  
  fetchFiscalYears(){
    this.fiscalYearService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.fiscalYears = jsonData;
      }
    );
  }
  
  //TODO:fix edit
  editFiscalYearCommand(fiscalYear: IFiscalYear){
    this.addFiscalYear = null;
    this.editFiscalYear = fiscalYear;
    this.fiscalYearDate = new Date(fiscalYear.start)
      .toLocaleDateString('en-GB', {day : 'numeric', month : 'numeric', year : 'numeric'});
    log.debug('statement', fiscalYear);
  }

  saveFiscalYear(fiscalYear: IFiscalYear):void{
    log.debug('statement', fiscalYear);
    this.fiscalYearService.put(fiscalYear!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          log.debug('response', response);
          this.editFiscalYear= null;
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  cancelSaveFiscalYear():void{
    this.addFiscalYear= null;
    this.editFiscalYear = null;
  }
  
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeFiscalYear(fiscalYear: IFiscalYear): void {
    this.fiscalYears= this.fiscalYears.filter(fy => fy !== fiscalYear);
    this.showedFiscalYears = this.fiscalYears;
  }

  getFiscalYears(){
    return this.showedFiscalYears;
  }

  getEditFiscalYear(){
    return this.editFiscalYear;
  }

  fiscalYearQueryChanged(){
    this.showedFiscalYears = this.fiscalYears.filter(fy => fy.projectName.includes(this.fiscalYearQuery));
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }
  
  attached() {
    log.debug('attached');
    if (this.companies.length == 0) {
      if (this.sharedService.getEntity().length == 0) {
        this.companyService.fetchAll().then(
          jsonData => {
            log.debug('jsonData', jsonData);
            this.companies = jsonData;
          }
        );
      } else {
        this.companies = this.sharedService.getEntity();
      }
      this.getFiscalYears();
      this.getEditFiscalYear();
    }
  }
  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
