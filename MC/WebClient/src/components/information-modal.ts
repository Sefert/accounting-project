import {LogManager, autoinject, View} from "aurelia-framework";
import {DialogController} from 'aurelia-dialog';
import {ICompany} from "../interfaces/ICompany";
import {IBaseEntity} from "../interfaces/IBaseEntity";
import {IStatementSheet} from "../interfaces/IStatementSheet";
import { DOM } from 'aurelia-pal';
import {DialogContainer} from "./dialog-container";

export var log = LogManager.getLogger('app.components.delete');

@autoinject
export class DeleteModal<TEntity extends IBaseEntity> {
  controller : DialogController;
  model : ICompany | IStatementSheet | TEntity;
  shownInformation : string;
  private dialogContainer: DialogContainer<HTMLElement>;
  private dialogOverlay: HTMLElement;
  private helpText: string;
  
  constructor(controller : DialogController){
    this.controller = controller;
    this.controller.settings.lock = false;
    this.controller.settings.centerHorizontalOnly = true;
  }
  activate(helpText: string){
  this.helpText = helpText;
  this.dialogOverlay = DOM.getElementById('note') as HTMLElement;
    this.controller.settings.position!(this.dialogOverlay, this.dialogOverlay);
  }
}
