import {LogManager, autoinject, View} from "aurelia-framework";
import {DialogController} from 'aurelia-dialog';
import {ICompany} from "../interfaces/ICompany";
import {IBaseEntity} from "../interfaces/IBaseEntity";
import {IStatementSheet} from "../interfaces/IStatementSheet";

export var log = LogManager.getLogger('app.components.delete');

@autoinject
export class DeleteModal<TEntity extends IBaseEntity> {
  controller : DialogController;
  //model : any;
  model : ICompany | IStatementSheet | TEntity;
  shownInformation : string;
  constructor(controller : DialogController){
    this.controller = controller;
  }
  activate(model : ICompany | IStatementSheet | TEntity){
    this.model = model;
    if ("name" in model){
      this.shownInformation = model.name
    } else if ("transactionCode" in model){
      this.shownInformation = model.transactionCode;
    }
  }
}
