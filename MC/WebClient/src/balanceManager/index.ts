import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {bindable, observable} from "aurelia-typed-observable-plugin";
import {IStatementSheet} from "../interfaces/IStatementSheet";
import {StatementSheetService} from "../services/statement-sheet-service";
import {CompanyService} from "../services/company-service";
import {ICompany} from "../interfaces/ICompany";


export var log = LogManager.getLogger('app.statements.index');

//https://www.tutorialspoint.com/aurelia/aurelia_dialog.html
@autoinject
export class Index {

  private statements: IStatementSheet[] = [];
  private showedStatements: IStatementSheet[] = [];
  private companies: ICompany[] = [];
  private hc: ICompany;
 
  
  @observable private editStatement : IStatementSheet | null;
  @observable private addStatement : IStatementSheet | null;
  @observable statementQuery : string;
  @observable companyQuery: string;
  @observable private errorReport: string | null = null;

  constructor
  (
    private statementService: StatementSheetService,
    private companyService: CompanyService,
    private dialogService: DialogService,
    private controller: DialogController,
  )
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openStatementDeleteModal(statement: IStatementSheet) {
    this.dialogService.open( {viewModel: DeleteModal, model: statement}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.statementService.delete(statement.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeStatement(statement);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    });
  }

  companyQueryChanged(){
    this.hc = this.getCompany()!;
    if (this.hc != undefined) {
      this.statementService.fetchAllByUrl('HoldingCompanies' + '/' + this.hc.id + '/' + 'StatementSheets').then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.statements = jsonData;
          this.showedStatements = this.statements;
          this.errorReport = null;
        });
    }
  }
  
  private getCompany(){
    return this.companies.find(c => c.name +" "+ c.vatRegistration == this.companyQuery);
  }
  
  createStatement(){
    try {
      this.errorReport = null;
      console.log(this.companyQuery);
      if(this.companyQuery != null){
        this.hc = this.getCompany()!;
        log.debug('company', this.hc);
        this.statements.unshift({
          "id" : 0,
          "tally" : "New Statement",
          "transactionCode" : "New Code",
          "holdingCompanyId" : this.hc.id,
          "HoldingCompany" : null,
        });
        this.addStatement = this.statements[0];
        this.editStatement = this.statements[0];
      } else {
        console.log("No company selected");
        this.errorReport = "Please select company!";
      }
    } catch (e) {
      this.errorReport = "Please select company!";
      console.log("Error");
    }
  }

  postStatement(statement: IStatementSheet){
    console.log(statement);
    this.statementService.post(statement).then(
      response =>{
        if (response.status == 201) {
          this.addStatement = null;
          this.editStatement = null;
        }else {
          log.error('Error in response', response);
        }
      }
    );
  }
  
  editStatementCommand(statement: IStatementSheet){
    this.addStatement = null;
    this.editStatement = statement;
    log.debug('statement', statement);
  }

  saveStatement(statement: IStatementSheet):void{
    log.debug('statement', statement);
    //this.editStatement = null;
    this.statementService.put(statement!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          log.debug('response', response);
          this.editStatement= null;
          //this.removeStatement(statement);
          //this.statements.push(statement);
          //this.showedStatements.push(statement);
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  
  
  cancelSaveStatement():void{
    this.addStatement = null;
    this.editStatement = null;
  }
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeStatement(statement: IStatementSheet): void {
    this.statements= this.statements.filter(c => c !== statement);
    this.showedStatements = this.statements;
  }

  getStatements(){
    return this.showedStatements;
  }

  getEditStatement(){
    return this.editStatement;
  }

  statementQueryChanged(){
    this.showedStatements = this.statements.filter(s => s.transactionCode.includes(this.statementQuery) || s.tally.includes(this.statementQuery));
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }
  
  attached(){
    log.debug('attached');
    if(this.companies.length == 0) {
      this.companyService.fetchAll().then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.companies = jsonData;
          //this.showedCompanies = this.companies;
        }
      );
    }
    this.getStatements();
    this.getEditStatement();
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
