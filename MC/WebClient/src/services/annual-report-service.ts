import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";
import {IAnnualReport} from "../interfaces/IAnnualReport";


export var log = LogManager.getLogger('app.companyService');

@autoinject
export class AnnualReportService extends BaseService<IAnnualReport> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'InvoiceManager');
  }
}
