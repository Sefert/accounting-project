import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {IBank} from "../interfaces/IBank";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";

export var log = LogManager.getLogger('app.banksService');

//httpclient programming in async
@autoinject
export class BankService extends BaseService<IBank> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Banks');
  }

}
