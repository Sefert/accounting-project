import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {ICompany} from "interfaces/ICompany";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";
import {IStatementSheet} from "../interfaces/IStatementSheet";
import {StatementSheetService} from "./statement-sheet-service";
import {DialogController, DialogService} from "aurelia-dialog";
import {SharedService} from "./shared-service";
import {DeleteModal} from "../components/delete-modal";


export var log = LogManager.getLogger('app.companyService');

@autoinject
export class CompanyService extends BaseService<ICompany> {
  
  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig,
  ) {
    super(httpClient, appConfig, 'HoldingCompanies');
  }

}
