import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";
import {ICommodity} from "../interfaces/ICommodity";


export var log = LogManager.getLogger('app.companyService');

@autoinject
export class CommodityService extends BaseService<ICommodity> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig,
  ) {
    super(httpClient, appConfig, 'Commodities');
  }

}
