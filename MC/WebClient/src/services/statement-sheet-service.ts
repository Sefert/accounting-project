import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {IStatementSheet} from "interfaces/IStatementSheet";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";


export var log = LogManager.getLogger('app.companyService');

@autoinject
export class StatementSheetService extends BaseService<IStatementSheet> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'StatementSheets');
  }

}
