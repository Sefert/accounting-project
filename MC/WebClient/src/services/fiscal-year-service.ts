import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IFiscalYear} from "../interfaces/IFiscalYear";

export var log = LogManager.getLogger('app.fiscalYearService');

//httpclient programming in async
@autoinject
export class FiscalYearService extends BaseService<IFiscalYear> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'FiscalYears');
  }

}
