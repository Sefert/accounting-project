import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";
import {IInvoiceManager} from "interfaces/IInvoiceManager";


export var log = LogManager.getLogger('app.companyService');

@autoinject
export class InvoiceManagerService extends BaseService<IInvoiceManager> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'InvoiceManager');
  }
}
