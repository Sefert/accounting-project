import {LogManager, autoinject} from "aurelia-framework";
import {BaseService} from "./base-service";
import {ICompany} from "interfaces/ICompany";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "app-config";


export var log = LogManager.getLogger('app.companyService.subsidiaries');

@autoinject
export class SubsidiaryService extends BaseService<ICompany> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'HoldingCompanies/Subsidiaries');
  }

}
