import {LogManager, autoinject, PLATFORM} from "aurelia-framework";
import {activationStrategy, RouterConfiguration, Router} from 'aurelia-router';
import {AppConfig} from "./app-config";

export var log = LogManager.getLogger('app.MainRouter');

@autoinject
export class MainRouter {
  
  private router: Router;
  
  //@autoinject gives appConfig
  constructor(private appConfig: AppConfig){
    log.debug('constructor');
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    log.debug('configureRouter');
    this.router = router;
    config.title = 'Accounting - Aurelia';
    if (this.appConfig == null){
      this.router.navigateToRoute('home');
    }
    config.map([
      
      {route: 'home', name: 'home', moduleId: PLATFORM.moduleName('home'), nav: true, title: 'Home'},
      
      {route: ['identity/login', '', 'index',], name: 'identity' + 'Login', moduleId: PLATFORM.moduleName('identity/login'), nav: false, title: 'Login'},
      {route: ['admin', 'identity/register'], name: 'identity' + 'Register', moduleId: PLATFORM.moduleName('identity/register'), nav: false, title: 'Register'},
      {route: 'identity/logout', name: 'identity' + 'Logout', moduleId: PLATFORM.moduleName('identity/logout'), nav: false, title: 'Logout'},
      
      {route: ['banks', 'banks/index'], name: 'banks' + 'Index', moduleId: PLATFORM.moduleName('banks/index'), nav: true, title: 'Banks'},
      //{route: 'banks/create', name: 'banks' + 'Create', moduleId: PLATFORM.moduleName('banks/create'), nav: false, title: 'Banks'},
      //{route: 'banks/edit/:id', name: 'banks' + 'Edit', moduleId: PLATFORM.moduleName('banks/edit'), nav: false, title: 'Banks'},
      //{route: 'banks/delete/:id', name: 'banks' + 'Delete', moduleId: PLATFORM.moduleName('banks/delete'), nav: false, title: 'Banks'},
      //{route: 'banks/details/:id', name: 'banks' + 'Details', moduleId: PLATFORM.moduleName('banks/details'), nav: false, title: 'Banks'},

      {route: ['companies', 'companies/index'], name: 'companies' + 'Index', moduleId: PLATFORM.moduleName('holdingCompanies/index'), nav: true, title: 'Companies'},
      {route: 'companies/subsidiaries/:id', name: 'companies' + 'Subsidiaries', moduleId: PLATFORM.moduleName('holdingCompanies/subsidiaries'), nav: false, title: 'HoldingCompanies'},
      {route: 'companies/create', name: 'company' + 'Create', moduleId: PLATFORM.moduleName('holdingCompanies/create'), nav: false, title: 'HoldingCompanies'},
      
      {route: ['annualReport', 'annualReport/index'], name: 'annualReport' + 'Index', moduleId: PLATFORM.moduleName('annualReport/index'), nav: true, title: 'Annual report'},
      
      ///{route: ['invoices', 'invoices/index'], name: 'invoices' + 'Index', moduleId: PLATFORM.moduleName('invoiceManager/index'), nav: true, title: 'Invoices'},

      {route: ['statements', 'statements/index'], name: 'statements' + 'Index', moduleId: PLATFORM.moduleName('statementManager/index'), nav: true, title: 'Account Setup'},
      
      {route: ['balance', 'balance/index'], name: 'balance' + 'Index', moduleId: PLATFORM.moduleName('balanceManager/index'), nav: true, title: 'Balance'},

      {route: ['fiscalYear', 'fiscalYears/index'], name: 'fiscalYears' + 'Index', moduleId: PLATFORM.moduleName('fiscalYearManager/index'), nav: true, title: 'FiscalYear'},

      {route: ['commodities', 'commodities/index'], name: 'commodities' + 'Index', moduleId: PLATFORM.moduleName('commoditiesManager/index'), nav: true, title: 'Commodities'},
    ]);
  }
  
}
  
