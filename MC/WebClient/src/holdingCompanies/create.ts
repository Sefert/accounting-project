import {LogManager, autoinject, View} from "aurelia-framework";
import {RouterConfiguration, NavigationInstruction, Router} from 'aurelia-router';
import {BankService} from "../services/bank-service";
import {IBank} from "../interfaces/IBank";
import {json} from "aurelia-fetch-client";
import {ICompany} from "../interfaces/ICompany";
import {CompanyService} from "../services/company-service";

export var log = LogManager.getLogger('app.holdingCompanies.create');

@autoinject
export class Create {
  
  private company: ICompany;

  constructor(private router: Router, private companyService: CompanyService){
    log.debug('constructor');
  }
  
  //====== View Methods ======
  submit(): void{
    log.debug('holdingCompany', this.company);
    this.companyService.post(this.company).then(
      response =>{
        if (response.status == 201) {
          //this.companyService.addCompany(this.company);
          this.router.navigateToRoute('companiesIndex');
        }else {
          log.error('Error in response', response);
        }
      }
    );
  }
  // ====== Lifecycle events =======
  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){

  }
  detached(){
    log.debug('detached');
  }
}
