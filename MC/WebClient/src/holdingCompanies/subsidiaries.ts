import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {ICompany} from "../interfaces/ICompany";
import {CompanyService} from "../services/company-service";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {observable} from "aurelia-typed-observable-plugin";
import {SubsidiaryService} from "../services/subsidiary-service";
import {ISubsidiary} from "../interfaces/ISubsidiary";


export var log = LogManager.getLogger('app.companies.index');

//https://www.tutorialspoint.com/aurelia/aurelia_dialog.html
@autoinject
export class Subsidiaries {

  private subsidiaries: ISubsidiary[] = [];
  @observable private editSubsidiary : ISubsidiary | null;

  constructor(
    private subsidiaryService: SubsidiaryService,
    private dialogService: DialogService,
    private controller: DialogController)
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openDeleteModal(subsidiary: ISubsidiary) {
    this.dialogService.open( {viewModel: DeleteModal, model: subsidiary}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.subsidiaryService.delete(subsidiary.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeSubsidiary(subsidiary);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    }).then();
  }

  editCompanyCommand(subsidiary: ISubsidiary){
    this.editSubsidiary = subsidiary;
    console.log(subsidiary);
  }

  saveCompany(subsidiary: ISubsidiary):void{
    log.debug('company', subsidiary);
    this.subsidiaryService.put(subsidiary!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          this.editSubsidiary = null;
          this.removeSubsidiary(subsidiary);
          this.subsidiaries.push(subsidiary);
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  cancelSaveCompany():void{
    this.editSubsidiary = null;
  }
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeSubsidiary(subsidiary: ISubsidiary): void {
    this.subsidiaries= this.subsidiaries.filter(c => c !== subsidiary);
  }

  getSubsidiaries(){
    return this.subsidiaries;
  }

  getEditSubsidiary(){
    return this.editSubsidiary;
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
    this.getSubsidiaries();
    this.getEditSubsidiary();
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    this.subsidiaryService.fetchAllById(params.id).then(
      subsidiaries => {
        log.debug('subsidiaries', subsidiaries);
        this.subsidiaries = subsidiaries;
      }
    );

    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
