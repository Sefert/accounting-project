import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {bindable, observable} from "aurelia-typed-observable-plugin";
import {IStatementSheet} from "../interfaces/IStatementSheet";
import {StatementSheetService} from "../services/statement-sheet-service";
import {CompanyService} from "../services/company-service";
import {ICompany} from "../interfaces/ICompany";
import {SharedService} from "../services/shared-service";
import {IBank} from "../interfaces/IBank";
import {BankService} from "../services/bank-service";


export var log = LogManager.getLogger('app.statements.index');

@autoinject
export class Index {

  private banks: IBank[] = [];
  private showedBanks: IBank[] = [];
  private companies: ICompany[] = [];
  private hc: ICompany;


  @observable private editBank : IBank | null;
  @observable private addBank : IBank | null;
  @observable bankQuery : string;
  @observable companyQuery: string;
  @observable private errorReport: string | null = null;

  constructor
  (
    private bankService: BankService,
    private companyService: CompanyService,
    private dialogService: DialogService,
    private controller: DialogController,
    private sharedService: SharedService<ICompany>
  )
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openBankDeleteModal(bank: IBank) {
    this.dialogService.open( {viewModel: DeleteModal, model: bank}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.bankService.delete(bank.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeBank(bank);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    });
  }

  companyQueryChanged(){
    this.hc = this.getCompany()!;
    if (this.hc != undefined) {
      this.bankService.fetchAllByUrl('HoldingCompanies' + '/' + this.hc.id + '/' + 'Banks').then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.banks = jsonData;
          this.showedBanks = this.banks;
          this.errorReport = null;
        });
    }
  }

  private getCompany(){
    return this.companies.find(c => c.name +" "+ c.vatRegistration == this.companyQuery);
  }

  createBankCommand(){
    try {
      this.errorReport = null;
      console.log(this.companyQuery);
      if(this.companyQuery != null){
        this.hc = this.getCompany()!;
        log.debug('company', this.hc);
        this.banks.unshift({
          "id" : 0,
          "name" : "New Name",
          "account" : "New Account",
          "iban" : "New IBAN",
          "swiftBic" : "New SWIFT",
          "holdingCompanyId" : this.hc.id
        });
        this.addBank = this.banks[0];
        this.editBank = this.banks[0];
      } else {
        console.log("No company selected");
        this.errorReport = "Please select company!";
      }
    } catch (e) {
      this.errorReport = "Please select company!";
      console.log("Error");
    }
  }

  postBank(bank: IBank){
    console.log(bank);
    this.bankService.post(bank).then(
      response =>{
        if (response.status == 201) {
          this.addBank = null;
          this.editBank = null;
          this.fetchBanks();
        }else {
          log.error('Error in response', response);
        }
      }
    );
  }

  editBankCommand(bank: IBank){
    this.addBank = null;
    this.editBank = bank;
    log.debug('statement', bank);
  }

  saveBank(bank: IBank):void{
    log.debug('statement', bank);
    //this.editStatement = null;
    this.bankService.put(bank!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          log.debug('response', response);
          this.editBank= null;
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  cancelSaveBank():void{
    this.addBank = null;
    this.editBank = null;
  }

  fetchBanks(){
    this.bankService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.banks = jsonData;
      }
    );
  }
  
  cancelPostBank(bank: IBank):void{
    this.addBank = null;
    this.editBank = null;
    this.removeBank(bank);
  }
  
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeBank(bank: IBank): void {
    this.banks= this.banks.filter(b => b !== bank);
    this.showedBanks = this.banks;
  }

  getBanks(){
    return this.showedBanks;
  }

  getEditBank(){
    return this.editBank;
  }

  bankQueryChanged(){
    this.showedBanks = this.banks.filter(b => b.name.includes(this.bankQuery) || b.account.includes(this.bankQuery));
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
    if(this.companies.length == 0) {
      if(this.sharedService.getEntity().length == 0) {
        this.companyService.fetchAll().then(
          jsonData => {
            log.debug('jsonData', jsonData);
            this.companies = jsonData;
            //this.showedCompanies = this.companies;
          }
        );
      } else{
        this.companies = this.sharedService.getEntity();
      }
    }
    this.getBanks();
    this.getEditBank();
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
