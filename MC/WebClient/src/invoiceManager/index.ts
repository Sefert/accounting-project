import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {ICompany} from "../interfaces/ICompany";
import {CompanyService} from "../services/company-service";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {observable} from "aurelia-typed-observable-plugin";
import {SharedService} from "../services/shared-service";


export var log = LogManager.getLogger('app.companies.index');

//https://www.tutorialspoint.com/aurelia/aurelia_dialog.html
@autoinject
export class Index {

  private companies: ICompany[] = [];
  private showedCompanies: ICompany[] = [];
  @observable private editCompany : ICompany | null;
  @observable query : any;

  constructor(
    private companyService: CompanyService,
    private dialogService: DialogService,
    private controller: DialogController,
    private sharedCompany: SharedService<ICompany>,
    private router: Router)
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openDeleteModal(company: ICompany) {
    this.dialogService.open( {viewModel: DeleteModal, model: company}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.companyService.delete(company.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeCompany(company);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    }).then();
  }
  

  editCompanyCommand(company: ICompany){
    this.editCompany = company;
    console.log(company);
  }

  saveCompany(company: ICompany):void{
    log.debug('company', company);
    this.companyService.put(company!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          this.editCompany = null;
          this.removeCompany(company);
          this.companies.push(company);
          this.showedCompanies.push(company);
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  cancelSaveCompany():void{
    this.editCompany = null;
  }
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeCompany(company: ICompany): void {
    this.companies= this.companies.filter(c => c !== company);
    this.showedCompanies = this.companies;
  }

  getCompanies(){
    return this.showedCompanies;
  }

  getEditCompany(){
    return this.editCompany;
  }

  queryChanged(){
    this.showedCompanies= this.companies.filter(c => c.name.includes(this.query));
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
    if(this.companies.length == 0) {
      this.companyService.fetchAll().then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.companies = jsonData;
          this.showedCompanies = this.companies;
        }
      );
    }
    this.getCompanies();
    this.getEditCompany();
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
