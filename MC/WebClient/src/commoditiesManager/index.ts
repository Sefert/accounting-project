import {DialogService, DialogController} from 'aurelia-dialog';
import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {DeleteModal} from "../components/delete-modal";
import {bindable, observable} from "aurelia-typed-observable-plugin";
import {IStatementSheet} from "../interfaces/IStatementSheet";
import {StatementSheetService} from "../services/statement-sheet-service";
import {CompanyService} from "../services/company-service";
import {ICompany} from "../interfaces/ICompany";
import {SharedService} from "../services/shared-service";
import {IBank} from "../interfaces/IBank";
import {BankService} from "../services/bank-service";
import {ICommodity} from "../interfaces/ICommodity";
import {CommodityService} from "../services/commodity-service";


export var log = LogManager.getLogger('app.commodities.index');

@autoinject
export class Index {

  private commodities: ICommodity[] = [];
  private showedCommodities: ICommodity[] = [];
  private companies: ICompany[] = [];
  private hc: ICompany;


  @observable private editCommodity : ICommodity | null;
  @observable private addCommodity : ICommodity | null;
  @observable commodityQuery : string;
  @observable companyQuery: string;
  @observable private errorReport: string | null = null;

  constructor
  (
    private commodityService: CommodityService,
    private companyService: CompanyService,
    private dialogService: DialogService,
    private controller: DialogController,
    private sharedService: SharedService<ICompany>
  )
  {
    this.controller = controller;
    log.debug('constructor');
  }

  openCommodityDeleteModal(commodity: ICommodity) {
    this.dialogService.open( {viewModel: DeleteModal, model: commodity}).whenClosed(openDialogResult => {
      console.log(openDialogResult);

      // Promise for the result is stored in openDialogResult.closeResult property
      if (!openDialogResult.wasCancelled) {
        this.commodityService.delete(commodity.id).then(response => {
          if (response.status == 204 || response.status == 200) {
            console.log('Deleted');
            this.removeCommodity(commodity);
            //this.router.navigateToRoute("companiesIndex");
          } else {
            log.debug('response', response);
          }
        });
      } else {
        console.log('cancelled');
        log.debug('response', openDialogResult);
      }
      log.debug('response', openDialogResult);
    });
  }

  companyQueryChanged(){
    this.hc = this.getCompany()!;
    if (this.hc != undefined) {
      this.commodityService.fetchAllByUrl('HoldingCompanies' + '/' + this.hc.id + '/' + 'Commodities').then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.commodities = jsonData;
          this.showedCommodities = this.commodities;
          this.errorReport = null;
        });
    }
  }

  private getCompany(){
    return this.companies.find(c => c.name +" "+ c.vatRegistration == this.companyQuery);
  }

  createCommodityCommand(){
    try {
      this.errorReport = null;
      console.log(this.companyQuery);
      if(this.companyQuery != null){
        this.hc = this.getCompany()!;
        log.debug('company', this.hc);
        this.commodities.unshift({
          "id" : 0,
          "itemName": "New Name",
          "netValue": 0,
          "unitType": "New Type",
          "holdingCompanyId": this.hc.id
        });
        this.addCommodity = this.commodities[0];
        this.editCommodity = this.commodities[0];
      } else {
        console.log("No company selected");
        this.errorReport = "Please select company!";
      }
    } catch (e) {
      this.errorReport = "Please select company!";
      console.log("Error");
    }
  }

  postCommodity(commodity: ICommodity){
    console.log(commodity);
    this.commodityService.post(commodity).then(
      response =>{
        if (response.status == 201) {
          this.addCommodity = null;
          this.editCommodity = null;
          this.fetchCommodities();
        }else {
          log.error('Error in response', response);
        }
      }
    );
  }

  editCommodityCommand(commodity: ICommodity){
    this.addCommodity = null;
    this.editCommodity = commodity;
    log.debug('statement', commodity);
  }

  saveCommodity(commodity: ICommodity):void{
    log.debug('statement', commodity);
    //this.editStatement = null;
    this.commodityService.put(commodity!).then(
      response => {
        if (response.status == 204 || response.status == 200){
          log.debug('response', response);
          this.editCommodity= null;
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  cancelSaveCommodity():void{
    this.addCommodity = null;
    this.editCommodity = null;
  }

  fetchCommodities(){
    this.commodityService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.commodities = jsonData;
      }
    );
  }
  
  cancelPostCommodity(commodity: ICommodity):void{
    this.addCommodity = null;
    this.editCommodity = null;
    this.removeCommodity(commodity);
  }
  
  //https://stackoverflow.com/questions/15292278/how-do-i-remove-an-array-item-in-typescript?rq=1
  removeCommodity(commodity: ICommodity): void {
    this.commodities= this.commodities.filter(b => b !== commodity);
    this.showedCommodities = this.commodities;
  }

  getComodities(){
    return this.showedCommodities;
  }

  getEditCommodity(){
    return this.editCommodity;
  }

  commodityQueryChanged(){
    this.showedCommodities = this.commodities.filter(b => b.unitType.includes(this.commodityQuery) || b.itemName.includes(this.commodityQuery));
  }

  created(owningView: View, myView: View){
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object){
    log.debug('bind');
  }

  attached(){
    log.debug('attached');
    if(this.companies.length == 0) {
      if(this.sharedService.getEntity().length == 0) {
        this.companyService.fetchAll().then(
          jsonData => {
            log.debug('jsonData', jsonData);
            this.companies = jsonData;
            //this.showedCompanies = this.companies;
          }
        );
      } else{
        this.companies = this.sharedService.getEntity();
      }
    }
    this.getComodities();
    this.getEditCommodity();
  }

  detached(){
    log.debug('detached');
  }

  unbind(){
    log.debug('unbind');
  }
  // ============= Router Events =============
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
